/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "FTLiteClusterMonitor.h"

// from boost
#include "AIDA/IHistogram1D.h"
#include "GaudiKernel/PhysicalConstants.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FTLiteClusterMonitor
//
// 2012-07-05 : Eric Cogneras
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTLiteClusterMonitor )

FTLiteClusterMonitor::FTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTLiteClusterLocation::Default},
                 KeyValue{"HitsLocation", LHCb::MCHitLocation::FT},
                 KeyValue{"LinkerLocation", Links::location( std::string( LHCb::FTLiteClusterLocation::Default ) +
                                                             "2MCHitsWithSpillover" )}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FTLiteClusterMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );

  if ( m_deFT == nullptr ) return Error( "Could not initialize DeFTDetector", StatusCode::FAILURE );
  if ( m_deFT->version() < 61 ) return Error( "This version requires FTDet v6.1 or higher", StatusCode::FAILURE );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void FTLiteClusterMonitor::operator()( const FTLiteClusters& clusters, const LHCb::MCHits& mcHits,
                                       const LHCb::LinksByKey& links ) const {

  // Store unique container of efficient MCHits
  std::set<LHCb::MCHit*> efficientMCHits;

  plot( clusters.size(), "nClusters", "Number of clusters; Clusters/event; Events", 0., 15.e3, 100 );

  // retrieve FTLiteClustertoMCHitLink
  auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>( links );

  uint prevSiPM = 0u, prevModuleID = 0u;
  int  clustersInSiPM = 0;

  // Loop over FTLiteCluster
  for ( const auto& cluster : clusters.range() ) {
    // Get the FTChannelID
    LHCb::FTChannelID chanID = cluster.channelID();
    // Get all MCHits linked to this (lite)cluster
    const auto mcHitLinks = myClusterToHitLink.from( chanID );

    // Get the correct module
    const DeFTModule* module = m_deFT->findModule( chanID );

    // Check if cluster is from pure spillover or from pure noise
    bool isSpillover = true;
    for ( const auto& imcHit : mcHitLinks ) {
      const auto mcHit = imcHit.to();
      if ( mcHit->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT ) {
        isSpillover = false;
        // Store this signal MCHit in list of efficient MCHits.
        efficientMCHits.insert( mcHit );
      }
    }
    std::string hitType = "";

    for ( auto i : {0, 1} ) {

      if ( i == 1 ) {
        if ( mcHits.empty() )
          hitType = "Noise/";
        else if ( isSpillover )
          hitType = "Spillover/";
        else
          hitType = "Signal/";
      }

      // draw cluster channel properties
      plot( (float)chanID.station(), hitType + "LiteClustersPerStation", "Lite clusters per station; Station; Clusters",
            0.5, 3.5, 3 );
      plot( (float)chanID.module(), hitType + "LiteClustersPerModule", "Lite clusters per module; Module; Clusters",
            -0.5, 5.5, 6 );
      plot( (float)chanID.sipmInModule(), hitType + "LiteClustersPerSiPM", "Lite clusters per SiPM; SiPMID; Clusters",
            0., 16., 16 );
      plot( (float)chanID.channel(), hitType + "LiteClustersPerChannel", "Lite clusters per channel; Channel; Clusters",
            -0.5, 127.5, 128 );

      if ( module != nullptr ) {
        int pseudoChannel = module->pseudoChannel( chanID );
        plot( pseudoChannel, hitType + "LiteClustersPerPseudoChannel",
              "Lite clusters per pseudo channel;Pseudo channel;Clusters/(64 channels)", 0., 12288., 192 );
      }

      plot( cluster.fraction(), hitType + "LiteClusterFraction", "Lite cluster fraction; Fraction", -0.25, 0.75, 2 );
      plot( cluster.pseudoSize(), hitType + "LiteClusterSize", "Lite cluster size; Size", -0.5, 8.5, 9 );
      plot2D( cluster.pseudoSize(), cluster.fraction(), hitType + "LiteClusterSizeVsFraction",
              "Cluster size vs fraction ; Size; Fraction", -0.5, 8.5, -0.25, 0.75, 9, 100 );
    }

    // Plots for cluster position resolution
    // Get the correct mat
    const DeFTMat* mat = module ? module->findMat( chanID ) : nullptr;

    if ( mat != nullptr ) {
      // Loop over all links to MCHits
      for ( const auto& imcHit : mcHitLinks ) {
        const auto mcHit  = imcHit.to();
        const auto mcPart = ( mcHit->mcParticle() );

        // Plot the resolution only for p > pMin
        if ( mcHit->p() < m_minPforResolution ) continue;
        if ( m_excludeSecondaries && mcPart->originVertex()->type() > 5 ) continue;

        // Plot the resolution only for the signal spill
        if ( mcHit->parent()->registry()->identifier() != "/Event/" + LHCb::MCHitLocation::FT ) continue;

        double dXCluster = mat->distancePointToChannel( mcHit->midPoint(), chanID, cluster.fraction() );
        double oriVtx =
            ( mcHit->mcParticle()->originVertex() == NULL ) ? 0 : mcHit->mcParticle()->originVertex()->position().Z();
        double oriVtxR = ( mcHit->mcParticle()->originVertex() == NULL )
                             ? 0
                             : std::sqrt( std::pow( mcHit->mcParticle()->originVertex()->position().X(), 2 ) +
                                          std::pow( mcHit->mcParticle()->originVertex()->position().Y(), 2 ) );

        plot( oriVtx, "Resolution/LiteClusteOriginZ",
              "Lite cluster origin vertex; Origin vertex [mm]; Number of clusters", -100, 12000, 500 );
        plot2D( oriVtx, oriVtxR, "Resolution/LiteClusterR_vs_z",
                "Lite cluster origin R vs origin z; Origin vertex z [mm]; Origin vertex r [mm]", -100., 12000., 0.,
                12000 * 0.4, 500, 500 );
        plot(
            dXCluster,
            "Resolution/LiteClusterResolution" +
                std::string( cluster.pseudoSize() == 4 ? "Small" : cluster.pseudoSize() > 4 ? "Large" : "Fragmented" ),
            "Lite cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );
        std::string clusSizeStr = std::to_string( cluster.pseudoSize() );
        plot( dXCluster, "Resolution/LiteClusterResolution" + clusSizeStr,
              "Lite cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );
        if ( std::abs( mcHit->mcParticle()->particleID().pid() ) == 11 )
          plot( dXCluster, "Resolution/LiteClusterResolutionElectron" + clusSizeStr,
                "Lite cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );
        else
          plot( dXCluster, "Resolution/LiteClusterResolutionNonElectron" + clusSizeStr,
                "Lite cluster resolution; Cluster - MCHit x position [mm]; Number of clusters", -1, 1, 100 );

        if ( std::abs( mcHit->mcParticle()->particleID().pid() ) == 11 ) continue;
        plot( dXCluster, "Resolution/LiteClusterResolutionSmall",
              "Lite cluster resolution small clusters; Cluster - MCHit x position [mm]; Number of clusters", -1, 1,
              100 );
        // Plot the angle of the cluster (dx) as function of its size
        Gaudi::XYZPoint localEntry = mat->geometry()->toLocal( mcHit->entry() );
        Gaudi::XYZPoint localExit  = mat->geometry()->toLocal( mcHit->exit() );
        float           tx         = ( localExit.x() - localEntry.x() ) / ( localExit.z() - localEntry.z() );
        float           theta      = atan( tx ) * 180 / Gaudi::Units::pi;
        float           ty         = ( localExit.y() - localEntry.y() ) / ( localExit.z() - localEntry.z() );
        float           phi        = atan( ty ) * 180 / Gaudi::Units::pi;
        float           dist       = ( localExit - localEntry ).R();
        plot( theta, "Resolution/MCHitTheta", "Theta of signal MCHit; #theta(MCHit) [#circ]; Number of clusters", -60.,
              60., 100 );
        plot( theta,
              "Resolution/MCHitTheta" + std::string( cluster.pseudoSize() == 4
                                                         ? "Small"
                                                         : cluster.pseudoSize() > 4 ? "Large" : "Fragmented" ),
              "Theta of signal MCHit; #theta(MCHit) [#circ]; Number of clusters", -60., 60., 100 );
        plot( phi, "Resolution/MCHitPhi", "Phi of signal MCHit; #phi(MCHit) [#circ]; Number of clusters", -60., 60.,
              100 );
        plot( dist, "Resolution/DistanceInMat", "Distance in fibres; #it{x}(MCHit) [mm]; Number of clusters", 0., 5.,
              100 );
        profile2D( mcHit->entry().x(), mcHit->entry().y(), dist, "Resolution/DistanceInMatXYmap",
                   "Distance in fibres; #it{x}(MCHit) [mm]; #it{y}(MCHit) [mm]; Distance", -3000., 3000., -2500., 2500.,
                   10, 10 );
        profile1D( theta, cluster.pseudoSize(), "Resolution/LiteClusterSizeVsTheta",
                   "LiteCluster size vs theta; #theta(MCHit) [#circ]; Cluster size", -60., 60., 20 );
        std::string thetaCut = std::to_string( 5 * int( 1 + std::abs( theta * 0.2 ) ) );
        plot( dXCluster, "Resolution/LiteClusterResolutionTheta" + thetaCut,
              "Lite cluster resolution #theta <" + thetaCut + "; Cluster - MCHit x position [mm]; Number of clusters",
              -1, 1, 100 );
        plot( dXCluster,
              "Resolution/LiteClusterResolution" + std::to_string( cluster.pseudoSize() ) + "Theta" + thetaCut,
              "Lite cluster resolution size " + std::to_string( cluster.pseudoSize() ) + " #theta <" + thetaCut +
                  "; Cluster - MCHit x position [mm]; Number of clusters",
              -1, 1, 100 );
        // Momentum vs origin vertex
        plot2D( oriVtx, mcHit->p(), "Resolution/LiteClusterMomentum_vs_z",
                "Lite cluster momentum vs origin z; Origin vertex z [mm]; Momentum [MeV/c^{2}]", -100., 12000.,
                m_minPforResolution, 5000., 500, 100 );
        plot1D( mcHit->p(), "Resolution/LiteClusterMomentum_size_" + clusSizeStr,
                "Lite cluster momentum;Momentum [MeV/c^{2}]", m_minPforResolution, 10000., 100 );
        // Angle vs cluster size
        plot2D( theta, cluster.pseudoSize(), "Resolution/LiteClusterSize_vs_angle_All",
                "Lite cluster size vs angle; Angle [°]; Size", 0., 90., -0.5, 8.5, 100, 9 );
        // Resolutions vs angles
        plot2D( theta, dXCluster, "Resolution/LiteClusterResolution_vs_angle_All",
                "Lite cluster resolution vs angle; Angle [°]; Cluster - MCHit x position [mm]", 0., 90., -1, 1, 100,
                100 );
        plot2D( theta, dXCluster, "Resolution/LiteClusterResolution_vs_angle_" + clusSizeStr,
                "Lite cluster resolution vs angle (size " + clusSizeStr +
                    "); Angle [°]; Cluster - MCHit x position [mm]",
                0., 90., -1, 1, 100, 100 );
        // Resolutions vs momentum
        plot2D( mcHit->p(), dXCluster, "Resolution/LiteClusterResolution_vs_P_All",
                "Lite cluster resolution vs momentum; P [MeV/c^{2}]; Cluster - MCHit x position [mm]",
                m_minPforResolution, 20000., -1, 1, 100, 100 );
        plot2D( mcHit->p(), dXCluster, "Resolution/LiteClusterResolution_vs_P_" + clusSizeStr,
                "Lite cluster resolution vs momentum (size " + clusSizeStr +
                    "); P [MeV/c^{2}]; Cluster - MCHit x position [mm]",
                m_minPforResolution, 20000., -1, 1, 100, 100 );
        // Angles vs momentum
        plot2D( mcHit->p(), theta, "Resolution/LiteClusterAngle_vs_P_All",
                "Lite cluster angle vs momentum; P [MeV/c^{2}]; Angle [°]", m_minPforResolution, 20000., 0., 90., 100,
                100 );
        plot2D( mcHit->p(), theta, "Resolution/LiteClusterResolution_vs_P_" + clusSizeStr,
                "Lite cluster angle vs momentum (size " + clusSizeStr + "); P [MeV/c^{2}] ; Angle [°]",
                m_minPforResolution, 20000., 0., 90., 100, 100 );
      }
    }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if ( thisSiPM != prevSiPM ) {
      if ( clustersInSiPM != 0 ) {
        plot( clustersInSiPM, "LiteClustersInSiPM", "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5,
              20.5, 21 );
        plot( clustersInSiPM, "LiteClustersInSiPM_Module" + std::to_string( prevModuleID ),
              "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );
        clustersInSiPM = 0;
      }
      prevSiPM     = thisSiPM;
      prevModuleID = chanID.module();
    }
    ++clustersInSiPM;
  }

  // Fill this for the last time
  plot( clustersInSiPM, "LiteClustersInSiPM", "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5,
        21 );
  plot( clustersInSiPM, "LiteClustersInSiPM_Module" + std::to_string( prevModuleID ),
        "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21 );

  // Efficiency plots
  int nMCHitsHighP = 0, nMCHitsHighPFound = 0;
  int nMCHits = 0, nMCHitsFound = 0;
  for ( auto const mcHit : mcHits ) {
    const auto mcPart = ( mcHit->mcParticle() );
    if ( m_excludeSecondaries && mcPart->originVertex()->type() > 5 ) continue;

    bool isEfficient = efficientMCHits.find( mcHit ) != efficientMCHits.end();
    for ( auto i : {0, 1} ) {
      if ( i == 1 && !isEfficient ) continue; // skip non-efficient hits in 2nd loop
      std::string histName = ( i == 1 ) ? "Found" : "";
      plot( mcHit->energy() / Gaudi::Units::MeV, "HitEfficiency/EnergyLossMCHit" + histName,
            "Energy loss of MCHit; #Delta#it{E} [MeV]; MCHits", 0., 1.0, 100 );
      plot( mcHit->time() / Gaudi::Units::ns, "HitEfficiency/TimeMCHit" + histName,
            "Time of flight of MCHit; #it{t}(MCHit) [ns]; MCHits", 20., 80.0, 60 );
      plot( mcHit->p() / Gaudi::Units::GeV, "HitEfficiency/MomentumMCHit" + histName,
            "Momentum of MCHit; #it{p} [GeV]; MCHits", 0., 20, 80 );

      if ( mcHit->p() > 5 * Gaudi::Units::GeV ) {
        if ( i == 0 )
          nMCHitsHighP++;
        else
          nMCHitsHighPFound++;
      }

      if ( i == 0 )
        nMCHits++;
      else
        nMCHitsFound++;
    }
  }

  // Print the cluster efficiency for all clusters and for those with p>5 GeV.
  if ( mcHits.size() != 0 ) {
    float clusEff = ( nMCHits != 0 ) ? 100. * float( nMCHitsFound ) / float( nMCHits ) : 0;

    plot( clusEff, "ClustersEfficiency", "Cluster efficiency ; Efficiency [%]; Events", 75.05, 100.05, 150 );

    float clusEffHighP = ( nMCHitsHighP != 0 ) ? 100. * float( nMCHitsHighPFound ) / float( nMCHitsHighP ) : 0;

    plot( clusEffHighP, "ClustersEfficiency5GeV", "Cluster efficiency p>5GeV; Efficiency [%]; Events", 85.05, 100.05,
          150 );

    if ( msgLevel( MSG::VERBOSE ) ) {
      debug() << "Cluster effiency (> 5 GeV) = " << format( "%4.1f", clusEff ) << "% ("
              << format( "%4.1f", clusEffHighP ) << "%)" << endmsg;
    }
  }

  return;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode FTLiteClusterMonitor::finalize() {

  AIDA::IHistogram1D* hEff5GeV = histo1D( HistoID( "ClustersEfficiency5GeV" ) );
  AIDA::IHistogram1D* hEff     = histo1D( HistoID( "ClustersEfficiency" ) );

  info() << " --------FT Lite clusters------------" << endmsg;
  if ( hEff != nullptr ) {
    info() << "Cluster efficiency  = ( " << format( "%4.1f", hEff->mean() ) << " +/- " << format( "%4.1f", hEff->rms() )
           << " )%" << endmsg;
  }
  if ( hEff5GeV != nullptr ) {
    info() << "Cluster efficiency p>5GeV = ( " << format( "%4.1f", hEff5GeV->mean() ) << " +/- "
           << format( "%4.1f", hEff5GeV->rms() ) << " )%" << endmsg;
  }
  info() << " -------------------------------" << endmsg;

  return Consumer::finalize(); // must be executed first
}
