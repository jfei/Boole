/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTPHOTONTOOL_H
#define MCFTPHOTONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiKernel/RndmGenerators.h"

// from CLHEP
#include "CLHEP/Random/RandPoissonQ.h"

// from FT/FTDigitisation
#include "MCFTCommonTools.h"

// Interface
#include "IMCFTPhotonTool.h"

/** @class MCFTPhotonTool MCFTPhotonTool.h
 *
 *  Interface for the tool that transforms deposited energies to photons exiting
 *  the fibre ends
 *
 *  @author Violaine Belle, Julian Wishahi
 *  @date   2016-09-23
 */

class MCFTPhotonTool : public extends<GaudiTool, IMCFTPhotonTool> {

public:
  using base_class::base_class;

  StatusCode initialize() override;

  double numExpectedPhotons( double effective_energy ) override;
  int    numObservedPhotons( double num_expected_photons ) override;
  double averagePropagationTime( double distToSiPM ) override { return distToSiPM * m_fibrePropagationSpeed; };

  double generateScintillationTime() override { return -log( m_rndmFlat() ) * m_scintillationDecayTime; }

  void generatePhoton( double& time, double& wavelength, double& posX, double& posZ, double& dXdY,
                       double& dZdY ) override;

private:
  /// Generate photon exit position and direction in fibre
  void generatePosAndDir( double& posX, double& posZ, double& dXdY, double& dZdY );

  // Random number generators
  Rndm::Numbers                        m_rndmFlat;
  Rndm::Numbers                        m_rndmLandau;
  Rndm::Numbers                        m_rndmGauss;
  MCFTCommonTools::HepRndmEngnIncptn   m_hepRndmEngnIncptn;
  std::unique_ptr<CLHEP::RandPoissonQ> m_rndmCLHEPPoisson;

  // Photon generation properties
  Gaudi::Property<float> m_photonsPerMeV{this, "PhotonsPerMeV", 6400., "Average number of photons per MeV"};
  Gaudi::Property<bool>  m_generateWavelength{this, "GenerateWavelength", false,
                                             "Flag for turning on random wavelength generation"};
  Gaudi::Property<float> m_wavelengthShift{this, "WavelengthShift", 405. * Gaudi::Units::nm,
                                           "Shift parameter of Wavelength distributions"};
  Gaudi::Property<float> m_wavelengthScale{this, "WavelengthScale", 77.89 * Gaudi::Units::nm,
                                           "Scale parameter of Wavelength distributions"};
  Gaudi::Property<float> m_wavelengthShape{this, "WavelengthShape", 0.44,
                                           "Shape parameter of Wavelength distributions"};
  Gaudi::Property<float> m_angleVar{this, "angleVar", 0.57, "Variance of normal distribution of photon exit angles."};

  // Fibre properties
  Gaudi::Property<float> m_effFibreR{this, "effectiveFibreRadius", 0.115 * Gaudi::Units::mm,
                                     "Effective Radius of the fibre"};
  Gaudi::Property<float> m_fibrePropagationSpeed{
      this, "FibrePropagationSpeed", 6.0 * Gaudi::Units::ns / Gaudi::Units::m, "Light propagation speed in fibre"};
  Gaudi::Property<float> m_scintillationDecayTime{this, "ScintillationDecayTime", 2.8 * Gaudi::Units::ns,
                                                  "Decay time of scintillation light release"};
};

#endif // MCFTPHOTONTOOL_H
