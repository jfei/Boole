/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonPhysicalChannelResponse.h"
#include "MuonDet/MuonReadoutCond.h"

MuonPhysicalChannelResponse::MuonPhysicalChannelResponse( Rndm::Numbers* flat, Rndm::Numbers* gauss,
                                                          Rndm::Numbers* genericPDF, Rndm::Numbers* electronicNoise,
                                                          double min, double max, MuonReadoutCond* detectorCondition,
                                                          int readoutType )
    : m_flat{flat}
    , m_gauss{gauss}
    , m_timeJitter{genericPDF}
    , m_electronicNoise{electronicNoise}
    , m_readoutIndex{readoutType}
    , m_minOfTimeJitter{min}
    , m_maxOfTimeJitter{max}
    , m_meadDeadtime{detectorCondition->meanDeadTime( readoutType )}
    , m_rmsDeadtime{detectorCondition->rmsDeadTime( readoutType )}
    , m_chamberEfficiency{detectorCondition->efficiency( readoutType )}
    , m_timeAdjustmentImprecision{detectorCondition->syncDrift( readoutType )}
    , m_detectorResponse{detectorCondition} {}

double MuonPhysicalChannelResponse::extractDeadtime() { return m_meadDeadtime + m_rmsDeadtime * ( *m_gauss )(); }

bool MuonPhysicalChannelResponse::surviveInefficiency() { return ( *m_flat )() <= m_chamberEfficiency; }

double MuonPhysicalChannelResponse::extractTimeAdjustmentImprecision() {
  return m_timeAdjustmentImprecision * ( *m_flat )();
}

double MuonPhysicalChannelResponse::extractTimeJitter() {
  const double offset = m_minOfTimeJitter;
  const double range  = m_maxOfTimeJitter - m_minOfTimeJitter;
  return offset + ( *m_timeJitter )() * range;
}
int MuonPhysicalChannelResponse::extractXTalkX( double distanceFromBoundary ) {
  return m_detectorResponse->singleGapClusterX( ( *m_flat )(), distanceFromBoundary, m_readoutIndex );
}
int MuonPhysicalChannelResponse::extractXTalkY( double distanceFromBoundary ) {
  return m_detectorResponse->singleGapClusterY( ( *m_flat )(), distanceFromBoundary, m_readoutIndex );
}
int MuonPhysicalChannelResponse::electronicNoise() { return ( *m_electronicNoise )(); }
