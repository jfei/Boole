/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTG4ATTENUATIONTOOL_H
#define MCFTG4ATTENUATIONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Kernel/STLExtensions.h"

#include "IMCFTAttenuationTool.h" // Interface

/** @class MCFTAttenuationTool MCFTAttenuationTool.h
 *
 *  Tool that reads attenutation maps from ConDB
 *
 *
 *  @author Martin Bieker based on implementation of M. Demmer and J. Wishahi
 *  @date   2015-01-15
 */

class MCFTG4AttenuationTool : public extends<GaudiTool, IMCFTAttenuationTool> {

public:
  using base_class::base_class;

  /// Initialize the transmission map
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  IMCFTAttenuationTool::Attenuation attenuation( double x, double y ) const override;

private:
  bool validateMap();
  int  findBin( LHCb::span<const double> axis, double position ) const;

  unsigned int        m_nBinsX, m_nBinsY;
  std::vector<double> m_xEdges, m_yEdges, m_effDir, m_effRef;

  // properties
  Gaudi::Property<double> m_mirrorReflectivity{this, "MirrorReflectivity", 0.75, // from LHCb-PUB-2014-020
                                               "Reflectivity of mirror at the end of the fibre mat (0-1)"};
  Gaudi::Property<int>    m_irradiation{this, "Irradiation", 50, "Simulated radiation damage"};
  Gaudi::Property<bool>   m_irradiationLinearModel{this, "IrradiationLinearModel", false,
                                                 "Radiation damage model (default is power-law model)"};
  Gaudi::Property<bool>   m_agingFibres{this, "AgingFibres", false, "Simulate time aging of the fibres"};
  Gaudi::Property<bool>   m_replaceModules{this, "ReplaceModules", false, "Simulate replacement of inner modules"};
};
#endif // MCFG4TATTENUATIONTOOL_H
