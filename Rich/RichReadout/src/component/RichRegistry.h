/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>
#include <set>
#include <string>
#include <vector>

#include "RichBase.h"

namespace LHCb {
  class RichSmartID;
}

namespace Rich::MC::Digi {

  /// Rich Registry utility class
  class RichRegistry {

  public:
    using BasePtr = std::shared_ptr<const RichBase>;

  public:
    static inline BasePtr GetNewBase( const LHCb::RichSmartID::Vector& pixels ) {
      auto b = std::make_shared<const RichBase>( pixels );
      theRegister.insert( b );
      return b;
    }

    inline static const RichBase* GetBase() {
      return ( !theRegister.empty() ? ( *theRegister.begin() ).get() : nullptr );
    }

  private: // data
    using RegisterMap = std::set<BasePtr, std::less<BasePtr>>;
    static RegisterMap theRegister;
  };

} // namespace Rich::MC::Digi
