/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/RndmGenerators.h"
#include <array>
#include <memory>
#include <vector>

class IRndmGenSvc;
class IDataProviderSvc;
class IMessageSvc;
class MuonPhysicalChannelResponse;
class MuonPhChID;
class MuonChamberResponse;
class DeMuonDetector;

class MuonDetectorResponse final {
public:
  // constructor
  MuonDetectorResponse( IRndmGenSvc* randSvc, IDataProviderSvc* detSvc, IMessageSvc* msgSvc );

  MuonPhysicalChannelResponse* getResponse( MuonPhChID phChID );
  MuonPhysicalChannelResponse* getResponse( int part, int readout ) { return responseVector[readout][part].get(); }
  MuonChamberResponse*         getChamberResponse( int part ) { return responseChamber[part].get(); }

private:
  Rndm::Numbers                                                               m_gaussDist;
  Rndm::Numbers                                                               m_flatDist;
  std::vector<std::unique_ptr<Rndm::Numbers>>                                 m_electronicNoise;
  std::vector<std::unique_ptr<Rndm::Numbers>>                                 m_timeJitter;
  std::array<std::array<std::unique_ptr<MuonPhysicalChannelResponse>, 20>, 2> responseVector;
  std::array<std::unique_ptr<MuonChamberResponse>, 20>                        responseChamber{};
  int                                                                         m_stationNumber{};
  int                                                                         m_regionNumber{};
  int                                                                         m_partition{};
  DeMuonDetector*                                                             m_muonDetector{nullptr};
};
