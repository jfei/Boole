/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file MCRichDigitSummaryAlg.h
 *
 *  Header file for algorithm : MCRichDigitSummaryAlg
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   04/10/2005
 */
//-----------------------------------------------------------------------------

#ifndef RICHMCASSOCIATORS_MCRichDigitSummaryAlg_H
#define RICHMCASSOCIATORS_MCRichDigitSummaryAlg_H 1

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichDigitSummary.h"

// base class
#include "RichKernel/RichAlgBase.h"

// interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

namespace Rich {
  namespace MC {

    //-----------------------------------------------------------------------------
    /** @class MCRichDigitSummaryAlg MCRichDigitSummaryAlg.h
     *
     *  Algorithm to fill the MCRichDigitSummary objects.
     *  Used to provide direct navigation from RichSmartIDs to MCParticles on the DST,
     *  and also to provide some history information.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   2005-10-04
     */
    //-----------------------------------------------------------------------------

    class MCRichDigitSummaryAlg final : public Rich::AlgBase {

    public:
      /// Standard constructor
      MCRichDigitSummaryAlg( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode initialize() override final; ///< Algorithm initialisation
      StatusCode execute() override final;    ///< Algorithm execution

    private: // data members
      /// Flag to turn off storing of spillover summaries
      bool m_storeSpill;

      /// Pointer to RichMCTruth tool
      const Rich::MC::IMCTruthTool* m_truth = nullptr;
    };

  } // namespace MC
} // namespace Rich

#endif // RICHMCASSOCIATORS_MCRichDigitSummaryAlg_H
