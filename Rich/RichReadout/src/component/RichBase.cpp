/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichBase.h"
#include "RichPixel.h"
#include "RichPixelReadout.h"
#include "RichRegistry.h"

using namespace Rich::MC::Digi;

void RichBase::upDate( const LHCb::RichSmartID::Vector& pixelList ) {
  clear();

  // CRJ : Until pixels have different properties, use the same for each
  auto props = std::make_shared<RichPixelProperties>();

  // Initialise new pixel map
  for ( const auto& index : pixelList ) { m_activePixels.emplace( index, props ); }

  CloseState();
}
