###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running Boole with default options
##############################################################################
# Syntax is:
#   gaudirun.py Boole-Default.py <someDataFiles>.py
##############################################################################

from Configurables import Boole

###############################################################################
# Set here any steering options.
# Available steering options and defaults are documented in
# $BOOLEROOT/python/Boole/Configuration.py
###############################################################################

# Just instantiate the configurable...
theApp = Boole()
##############################################################################
# I/O datasets are defined in a separate file, see examples in MC09-Files.py
##############################################################################
