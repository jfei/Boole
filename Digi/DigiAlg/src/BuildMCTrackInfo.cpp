/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FTLiteCluster.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "Event/VPDigit.h"

#include "Event/MCProperty.h"
#include "Linker/LinkedTo.h"

// Det
#include "FTDet/DeFTDetector.h"
#include "UTDet/DeUTDetector.h"
#include "VPDet/DeVP.h"

// local
#include "BuildMCTrackInfo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BuildMCTrackInfo
//
// 2004-01-08 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BuildMCTrackInfo )

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode BuildMCTrackInfo::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  if ( m_withVP ) m_vpDet = getDet<DeVP>( DeVPLocation::Default );
  if ( m_withUT ) {
    m_utDet          = getDet<DeUTDetector>( DeUTDetLocation::UT );
    m_utClustersName = LHCb::UTClusterLocation::UTClusters;
    m_utHitsName     = LHCb::MCHitLocation::UT;
  }
  if ( m_withFT ) m_ftDet = getDet<DeFTDetector>( DeFTDetectorLocation::Default );

  if ( msgLevel( MSG::DEBUG ) ) {
    if ( m_withUT ) debug() << "Number of UT layers " << m_utDet->layers().size() << endmsg;
    if ( m_withFT ) debug() << "Number of FT stations " << m_ftDet->nStations() << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode BuildMCTrackInfo::execute() {
  const bool isDebug   = msgLevel( MSG::DEBUG );
  const bool isVerbose = msgLevel( MSG::VERBOSE );

  if ( isDebug ) debug() << "==> Execute" << endmsg;

  LHCb::MCParticles* mcParts = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );

  LHCb::MCProperty* trackInfo = new LHCb::MCProperty();
  put( trackInfo, LHCb::MCPropertyLocation::TrackInfo );

  //== The array size is bigger than MCParticle.size() as the MCParticles are
  //== compressed, uninteresting particles were removed at the end of Brunel.
  int highestKey = mcParts->size();
  for ( LHCb::MCParticles::const_iterator itP = mcParts->begin(); mcParts->end() != itP; itP++ ) {
    if ( highestKey < ( *itP )->key() ) highestKey = ( *itP )->key();
  }
  unsigned int sizePart = highestKey + 1;

  if ( isDebug ) debug() << "Highest MCParticle number " << highestKey << endmsg;

  std::vector<int> lastVelo( sizePart, -1 );
  std::vector<int> veloPix( sizePart, 0 );
  std::vector<int> station( sizePart, 0 );

  LHCb::MCParticle* part;
  unsigned int      MCNum;

  if ( m_withVP ) {
    LHCb::VPDigits*            digits = get<LHCb::VPDigits>( LHCb::VPDigitLocation::Default );
    LinkedTo<LHCb::MCParticle> vpLink( eventSvc(), msgSvc(), LHCb::VPDigitLocation::Default );
    if ( vpLink.notFound() ) return StatusCode::FAILURE;

    for ( LHCb::VPDigits::const_iterator vIt = digits->begin(); digits->end() != vIt; ++vIt ) {
      int module = ( *vIt )->channelID().module();
      part       = vpLink.first( ( *vIt )->key() );
      while ( NULL != part ) {
        if ( mcParts == part->parent() ) {
          MCNum = part->key();
          if ( veloPix.size() > MCNum ) {
            if ( module != lastVelo[MCNum] ) {
              // Count only once per module a given MCParticle
              lastVelo[MCNum] = module;
              veloPix[MCNum]++;
              if ( isVerbose ) {
                verbose() << "MC " << MCNum << " VP module " << module << " nbVP " << veloPix[MCNum] << endmsg;
              }
            }
          }
        }
        part = vpLink.next();
      }
    }
  }

  if ( m_withUT ) { //== UT cluster -> particle associaton
    LHCb::UTClusters*                           UTDig = get<LHCb::UTClusters>( m_utClustersName );
    LinkedTo<LHCb::MCParticle, LHCb::UTCluster> utLink( eventSvc(), msgSvc(), m_utClustersName );
    if ( utLink.notFound() ) return StatusCode::FAILURE;

    for ( LHCb::UTClusters::const_iterator uIt = UTDig->begin(); UTDig->end() != uIt; uIt++ ) {
      int  sta = ( *uIt )->channelID().station() - 1; // 0-1 from 1-2
      int  lay = ( ( *uIt )->channelID().layer() - 1 ) % 4;
      bool isX = ( 0 == lay ) || ( 3 == lay );
      part     = utLink.first( *uIt );
      while ( NULL != part ) {
        if ( mcParts == part->parent() ) {
          MCNum = part->key();
          updateBit( station[MCNum], sta, isX );
          if ( isVerbose ) verbose() << "MC " << MCNum << " UT Sta " << sta << " lay " << lay << endmsg;
        }
        part = utLink.next();
      }
    }
  }

  if ( m_withFT ) { //=== Fibre Tracker
    using FTLiteClusters                = LHCb::FTLiteCluster::FTLiteClusters;
    FTLiteClusters*            clusters = get<FTLiteClusters>( LHCb::FTLiteClusterLocation::Default );
    LinkedTo<LHCb::MCParticle> ftLink( eventSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );

    if ( ftLink.notFound() ) return StatusCode::FAILURE;

    for ( const auto cluster : clusters->range() ) {
      LHCb::FTChannelID channelID = cluster.channelID();
      // Create station to be in the range 2-4
      int sta = channelID.station() + 1;
      int lay = channelID.layer();
      part    = ftLink.first( channelID );
      while ( part != nullptr ) {
        if ( mcParts == part->parent() ) {
          MCNum = part->key();
          updateBit( station[MCNum], sta, channelID.isX() );
          if ( isVerbose ) verbose() << "MC " << MCNum << " FT Sta " << sta << " lay " << lay << endmsg;
        }
        part = ftLink.next();
      }
    }
  }

  //=====================================================================
  // Now compute the acceptance, with MCHits
  //=====================================================================
  computeAcceptance( station );

  //== Build now the trackInfo tracks

  for ( LHCb::MCParticles::const_iterator itP = mcParts->begin(); mcParts->end() != itP; itP++ ) {
    MCNum    = ( *itP )->key();
    int mask = station[MCNum];
    if ( m_withVP ) {
      if ( 2 < veloPix[MCNum] ) mask |= MCTrackInfo::maskHasVelo;
      if ( 15 < veloPix[MCNum] ) veloPix[MCNum] = 15;
      mask |= ( veloPix[MCNum] << MCTrackInfo::multVeloR );
    }

    if ( 0 != mask ) {
      trackInfo->setProperty( *itP, mask );
      if ( isDebug ) {
        debug() << format( "Track %4d mask %8x nPix %2d nPhi %2d ", MCNum, mask, veloPix[MCNum] );
        if ( MCTrackInfo::maskHasVelo == ( mask & MCTrackInfo::maskHasVelo ) ) debug() << " hasVelo ";
        if ( MCTrackInfo::maskHasTT == ( mask & MCTrackInfo::maskHasTT ) ) debug() << " hasTT ";
        if ( MCTrackInfo::maskHasT == ( mask & MCTrackInfo::maskHasT ) ) debug() << " hasT ";
        debug() << endmsg;
      }
    }
  }

  return StatusCode::SUCCESS;
}

//=========================================================================
//  Process the MC(Velo)Hits to get the 'acceptance'
//=========================================================================
void BuildMCTrackInfo::computeAcceptance( std::vector<int>& station ) {

  const bool isDebug = msgLevel( MSG::DEBUG );
  if ( m_withVP ) {
    std::vector<int> nVP( station.size(), 0 );
    // LHCb::MCHits* veloHits = get<LHCb::MCHits>( LHCb::MCHitLocation::VP);
    LHCb::MCHits* veloHits = get<LHCb::MCHits>( LHCb::MCHitLocation::VP );
    for ( LHCb::MCHits::const_iterator vHit = veloHits->begin(); veloHits->end() != vHit; vHit++ ) {
      unsigned int MCNum = ( *vHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;
      nVP[MCNum]++;
    }
    for ( unsigned int MCNum = 0; station.size() > MCNum; MCNum++ ) {
      if ( 2 < nVP[MCNum] ) station[MCNum] |= MCTrackInfo::maskAccVelo;
    }
  }

  if ( m_withUT ) { //== UT
    LHCb::MCHits* utHits = get<LHCb::MCHits>( m_utHitsName );
    for ( LHCb::MCHits::const_iterator uHit = utHits->begin(); utHits->end() != uHit; uHit++ ) {
      unsigned int MCNum = ( *uHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;

      Gaudi::XYZPoint midPoint = ( *uHit )->midPoint();
      DeUTLayer*      utLay    = m_utDet->findLayer( midPoint );
      if ( 0 == utLay ) {
        if ( isDebug )
          debug() << format( "UT Hit not in any LAYER ! x %8.2f y%8.2f z%9.2f", midPoint.x(), midPoint.y(),
                             midPoint.z() )
                  << endmsg;
        continue;
      }
      int  sta = utLay->elementID().station() - 1;
      bool isX = utLay->angle() == 0;
      updateAccBit( station[MCNum], sta, isX );
    }
  }

  if ( m_withFT ) { //===Fibre Tracker
    LHCb::MCHits* ftHits = get<LHCb::MCHits>( LHCb::MCHitLocation::FT );
    for ( LHCb::MCHits::const_iterator fHit = ftHits->begin(); ftHits->end() != fHit; fHit++ ) {
      unsigned int MCNum = ( *fHit )->mcParticle()->key();
      if ( station.size() <= MCNum ) continue;
      Gaudi::XYZPoint    midPoint = ( *fHit )->midPoint();
      const DeFTLayer*   ftLay    = m_ftDet->findLayer( midPoint );
      const DeFTStation* ftSta    = m_ftDet->findStation( midPoint );
      if ( 0 == ftLay ) {
        if ( isDebug )
          debug() << format( "FT Hit not in any LAYER ! x %8.2f y%8.2f z%9.2f", midPoint.x(), midPoint.y(),
                             midPoint.z() )
                  << endmsg;
        continue;
      }
      int  sta = ftSta->stationID(); // station 1-3, layerID 0-3 xuvx
      bool isX = ftLay->stereoAngle() == 0;
      updateAccBit( station[MCNum], sta, isX );
    }
  }
}
//=============================================================================
