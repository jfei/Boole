/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichCopySummedDepositsToDigits.h
 *
 *  Header file for RICH digitisation algorithm : Rich::MC::Digi::CopySummedDepositsToDigits
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   18/11/2011
 */
//===============================================================================

#ifndef RICHREADOUT_RichCopySummedDepositsToDigits_H
#define RICHREADOUT_RichCopySummedDepositsToDigits_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

// event model
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class CopySummedDepositsToDigits RichCopySummedDepositsToDigits.h
       *
       *  Performs a simple simulation of the RICH HPD pixel response
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   18/11/2011
       */

      class CopySummedDepositsToDigits final : public Rich::AlgBase {

      public:
        /// Constructor
        CopySummedDepositsToDigits( const std::string& name, ISvcLocator* pSvcLocator );

        /// execute
        StatusCode execute() override final;

      private: // data
        std::string m_mcRichSummedDepositsLocation;
        std::string m_mcRichDigitsLocation;
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHREADOUT_RichCopySummedDepositsToDigits_H
