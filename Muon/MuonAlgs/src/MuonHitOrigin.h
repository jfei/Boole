/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MuonBXFlag.h"
#include "Event/MuonOriginFlag.h"
#include "MuonHitOriginPack.h"

class MuonHitOrigin {
public:
  [[nodiscard]] bool getGeantNature() const { return check<LHCb::MuonOriginFlag::GEANT>(); }
  [[nodiscard]] bool getBackgroundNature() const { return check<LHCb::MuonOriginFlag::BACKGROUND>(); }
  [[nodiscard]] bool getFlatSpilloverNature() const { return check<LHCb::MuonOriginFlag::FLATSPILLOVER>(); }
  [[nodiscard]] bool getChamberNoiseNature() const { return check<LHCb::MuonOriginFlag::CHAMBERNOISE>(); }
  [[nodiscard]] bool getXTalkNature() const { return check<LHCb::MuonOriginFlag::XTALK>(); }
  [[nodiscard]] bool getElectronicNoiseNature() const { return check<LHCb::MuonOriginFlag::ELECTRONICNOISE>(); }

  [[nodiscard]] unsigned int getBX() const;
  [[nodiscard]] unsigned int getNature() const;

  MuonHitOrigin& setHitNature( unsigned int value );
  MuonHitOrigin& setBX( unsigned int value );

private:
  unsigned int m_hitOrigin;

  template <LHCb::MuonOriginFlag::OriginFlag flag>
  bool check() const {
    return MuonHitOriginPack::getBit( m_hitOrigin, MuonHitOriginPack::shiftHitOrigin,
                                      MuonHitOriginPack::maskHitOrigin ) == flag;
  }
};

inline unsigned int MuonHitOrigin::getNature() const {
  return MuonHitOriginPack::getBit( m_hitOrigin, MuonHitOriginPack::shiftHitOrigin, MuonHitOriginPack::maskHitOrigin );
}

inline unsigned int MuonHitOrigin::getBX() const {
  return MuonHitOriginPack::getBit( m_hitOrigin, MuonHitOriginPack::shiftBX, MuonHitOriginPack::maskBX );
}

inline MuonHitOrigin& MuonHitOrigin::setHitNature( unsigned int value ) {
  MuonHitOriginPack::setBit( m_hitOrigin, MuonHitOriginPack::shiftHitOrigin, MuonHitOriginPack::maskHitOrigin, value );
  return *this;
}

inline MuonHitOrigin& MuonHitOrigin::setBX( unsigned int value ) {
  MuonHitOriginPack::setBit( m_hitOrigin, MuonHitOriginPack::shiftBX, MuonHitOriginPack::maskBX, value );
  return *this;
}
