/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MCFTDigitCreator MCFTDigitCreator.h
 *
 *  From the list of MCFTDeposit (deposited energy in each FTChannel),
 *  this algorithm converts the deposited energy in ADC charge according
 *  to the mean number of photoelectron per MIP
 *  + the mean ADC count per photoelectron.
 *  Created digits are put in transient data store.
 *
 *  TO DO :
 *  - add noise
 *
 *  THINK ABOUT :
 *  - dead channels
 *
 *  @author COGNERAS Eric, Luca Pescatore
 *  @date   2012-04-04
 */

#ifndef MCFTDIGITCREATOR_H
#define MCFTDIGITCREATOR_H 1

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/RndmGenerators.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/MCFTDeposit.h"
#include "Event/MCFTDigit.h"

// local
#include "SiPMResponse.h"

class MCFTDigitCreator : public Gaudi::Functional::Transformer<LHCb::MCFTDigits( const LHCb::MCFTDeposits& )> {

public:
  /// Standard constructor

  MCFTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode       initialize() override;
  LHCb::MCFTDigits operator()( const LHCb::MCFTDeposits& deposits ) const override;

private:
  SiPMResponse*         m_SiPMResponse = nullptr; ///< pointer to SiPM integrated response function
  mutable Rndm::Numbers m_gauss;

  Gaudi::Property<float> m_sipmGainVariation{this, "SiPMGainVariation", 0.05, "relative fluctuation of the gain"};
  Gaudi::Property<float> m_adcNoise{this, "ADCNoise", 0.1, "Sigma of the noise in the electronics ADC conversion"};
  Gaudi::Property<std::vector<double>> m_integrationOffset{
      this,
      "IntegrationOffset",
      {26 * Gaudi::Units::ns, 28 * Gaudi::Units::ns, 30 * Gaudi::Units::ns},
      "Vector of the integration offsets for T1,T2,T3"};
  Gaudi::Property<float> m_adcThreshold1{this, "ADCThreshold1", 1.5, "PE low threshold"};
  Gaudi::Property<float> m_adcThreshold2{this, "ADCThreshold2", 2.5, "PE mid threshold"};
  Gaudi::Property<float> m_adcThreshold3{this, "ADCThreshold3", 4.5, "PE high threshold"};
  Gaudi::Property<bool>  m_ignoreZeroADCDigits{this, "IgnoreZeroADCDigits", true, "Do not write digits when ADC <= 0"};
};
#endif // MCFTDIGITCREATOR_H
