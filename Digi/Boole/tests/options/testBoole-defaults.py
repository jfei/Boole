###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Default settings for all Boole tests
from Configurables import Boole, CondDB

CondDB().Upgrade = True
Boole().EvtMax = 10
Boole().Outputs = ["DIGI", "MDF"]  # Test all output types
Boole().Monitors = ["SC", "FPE"]  # Add StatusCode and FPE checks
Boole().DisableTiming = True  # Do not print timing table
