###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
####################################################################################
# Date      : 16-06-2014, updated 03-03-2017
# Purpose   : Run a cleanup job using Boole
#             to make a digi file out of an xdst.
# Author    : SciFi group
# Version   : this file was updated for Boole v31r1

# Questions : Please contact lhcb-upgrade-ft-software@cern.ch

###################################################################################

from Gaudi.Configuration import *
from Configurables import LHCbApp, CondDB

CondDB().Upgrade = True
LHCbApp().Simulation = True
importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/xdigi.py")

from Configurables import Boole
Boole().DetectorLink = ['VP', 'UT', 'FT', 'Tr', 'Magnet']
Boole().DetectorDigi = ['VP', 'UT', 'FT', 'Magnet']
Boole().DetectorMoni = ['VP', 'UT', 'FT', 'Magnet']
Boole().DataType = "Upgrade"
Boole().Outputs = ["DIGI"]
Boole().DigiType = 'Extended'
Boole().EvtMax = 10
Boole().DatasetName = 'reprocess'
Boole().InputDataType = 'XDST'  ## add
