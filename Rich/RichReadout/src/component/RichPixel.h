/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>
#include <vector>

#include "Kernel/RichSmartID.h"
#include "RichBase.h"
#include "RichPixelProperties.h"
#include "RichPixelReadout.h"
#include "RichRegistry.h"

namespace Rich::MC::Digi {

  /// Rich Pixel
  class RichPixel {

  public:
    // Basic constructor, takes a base and an index
    RichPixel( const std::shared_ptr<RichPixelProperties>& p ) : m_property( p ) {}

    RichPixel( const LHCb::RichSmartID sid ) {
      if ( sid.rich() == 0 ) // CRJ ??? Looks wrong to me...
      {
        auto b = RichRegistry::GetBase();
        if ( b ) { m_property = b->DecodeUniqueID( sid ); }
      }
    }

    RichPixel( const RichBase* b, const LHCb::RichSmartID sid ) : m_property( b->DecodeUniqueID( sid ) ) {}

    decltype( auto ) Readout() const { return m_property->Readout(); }

  private: // methods
    decltype( auto ) Property() const { return m_property; }

  private: // data
    std::shared_ptr<RichPixelProperties> m_property;

    friend class RichBase;
  };

} // namespace Rich::MC::Digi
