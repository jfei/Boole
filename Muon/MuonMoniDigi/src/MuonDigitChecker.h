/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MuonDigitChecker_H
#define MuonDigitChecker_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "MuonDet/MuonBasicGeometry.h"

/** @class MuonDigitChecker MuonDigitChecker.h
 *
 *
 *  @author A Sarti
 *  @date   2005-05-20
 */
class MuonDigitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  MuonDigitChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; /// initialization
  StatusCode execute() override;    /// execution
  StatusCode finalize() override;   /// finalization

private:
  int m_nhit[5][4][4], m_cnt[5][4][4];
  int m_nDhit[5][4][6], m_Dcnt[5][4][6];

  bool                               m_hitMonitor;
  std::unique_ptr<MuonBasicGeometry> m_base;
};
#endif // MuonDigitChecker_H
