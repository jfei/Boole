/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCHit.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/MsgStream.h"
#include "MuonDet/MuonBasicGeometry.h"

template <class T>
class MuonDigitizationData final {
public:
  // add or remove a pointer of a T object to the ith data partition
  T*   addMuonObject( int i, std::unique_ptr<T> pO );
  void eraseMuonObject( int i, T* pO );

  template <class Predicate>
  T* findObjectIn( int i, Predicate pred ) const {
    const auto* part = m_partition[i];
    auto        iter = std::find_if( part->begin(), part->end(), pred );
    return iter != part->end() ? *iter : nullptr;
  }
  StatusCode registerPartitions( IDataProviderSvc*, MuonBasicGeometry* );

  const auto& getPartition( int i ) const { return *m_partition[i]; }

  // standard constructor
  MuonDigitizationData( std::string path, MsgStream* );

  // standard destructor
  ~MuonDigitizationData();

  MuonDigitizationData( const MuonDigitizationData& ) = delete;
  MuonDigitizationData( MuonDigitizationData&& )      = delete;
  MuonDigitizationData& operator=( const MuonDigitizationData& ) = delete;
  MuonDigitizationData& operator=( MuonDigitizationData&& ) = delete;

private:
  std::array<KeyedContainer<T>*, 20> m_partition{};
  std::string                        m_path;
  int                                m_registered{0};
  MsgStream*                         m_log{nullptr};
};

using MCMuonHits = KeyedContainer<LHCb::MCHit, Containers::HashMap>;
