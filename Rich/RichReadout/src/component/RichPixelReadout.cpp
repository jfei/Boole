/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichPixelReadout.h"

#include "RichFrontEndDigitiser.h"
#include "RichShape.h"

using namespace Rich::MC::Digi;

RichPixelReadout::RichPixelReadout() : m_shape( new RichShape( 25, 2.7 ) ), m_frontEnd( new RichFrontEndDigitiser() ) {

  // const double peakTime    = 25.0;
  // const double calib       = 4.42;
  // const double threshold   = 8.8;
  // m_shape      = new RichShape( peakTime, 2.7 );
  // m_frontEnd   = new RichFrontEndDigitiser( threshold * m_sigmaElecNoise, calib );
}

RichPixelReadout::~RichPixelReadout() {
  delete m_shape;
  delete m_frontEnd;
}
