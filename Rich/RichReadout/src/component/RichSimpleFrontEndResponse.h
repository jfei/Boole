/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichSimpleFrontEndResponse.h
 *
 *  Header file for RICH digitisation algorithm : Rich::MC::Digi::SimpleFrontEndResponse
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#ifndef RICHREADOUT_RICHSIMPLEFRONTENDRESPONSE_H
#define RICHREADOUT_RICHSIMPLEFRONTENDRESPONSE_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"

// interfaces
#include "RichInterfaces/IRichSmartIDTool.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSmartID.h"

// local
#include "RichBase.h"
#include "RichFrontEndDigitiser.h"
#include "RichPixel.h"
#include "RichPixelReadout.h"
#include "RichRegistry.h"
#include "RichShape.h"
#include "RichTimeSample.h"

// event model
#include "Event/MCRichDeposit.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class SimpleFrontEndResponse RichSimpleFrontEndResponse.h
       *
       *  Performs a simple simulation of the RICH HPD pixel response
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @author Alex Howard   a.s.howard@ic.ac.uk
       *  @date   2003-11-06
       */

      class SimpleFrontEndResponse final : public Rich::AlgBase {

      public:
        /// Constructor
        SimpleFrontEndResponse( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override final;
        StatusCode finalize() override final;
        StatusCode execute() override final;

      private: // methods
        /// Run the Simple treatment
        StatusCode Simple();

      private: // data
        RichRegistry::BasePtr actual_base;
        RichRegistry          theRegistry;

        LHCb::MCRichSummedDeposits* SummedDeposits = nullptr;

        std::string m_mcRichSummedDepositsLocation;
        std::string m_mcRichDigitsLocation;

        int m_Baseline;
        int m_AdcCut;

        double m_Sigma;
        double m_Calibration;

        Rndm::Numbers m_gaussRndm;
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHREADOUT_RICHSIMPLEFRONTENDRESPONSE_H
