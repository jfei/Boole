/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BCMDIGI_BCMDIGITIZATION_H
#define BCMDIGI_BCMDIGITIZATION_H 1

#include <string>

// From Gaudi
#include "Event/BcmDigit.h"
#include "Event/MCHit.h"
#include "GaudiAlg/GaudiAlgorithm.h"

// local

// Event model

// Forward declaration
// class DeBcm;

class BcmDigitization : public GaudiAlgorithm {
public:
  // Constructor: A constructor of this form must be provided.
  BcmDigitization( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode execute() override;

private:
  //  std::string m_bcmUpDetLocation;
  //  std::string m_bcmDownDetLocation;
  std::string m_bcmHitsLocation;
  std::string m_bcmDigitLocation;
  std::string m_bcmMCDigitLocation;
  //  DeBcm* m_bcmUpDet;
  //  DeBcm* m_bcmDownDet;
  bool m_bcmMCDigits;
};

#endif
