/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTDEPOSITMONITOR_H
#define MCFTDEPOSITMONITOR_H 1

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

// from Event
#include "Event/MCFTDeposit.h"
#include "Event/MCHit.h"

// FTDet
#include "FTDet/DeFTDetector.h"

/** @class MCFTDepositMonitor MCFTDepositMonitor.h
 *
 *  @author Jeroen van Tilburg, Luca Pescatore
 *  @date   2017-03-23
 */
class MCFTDepositMonitor : public Gaudi::Functional::Consumer<void( const LHCb::MCFTDeposits& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public:
  MCFTDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTDeposits& deposits ) const override;

private:
  void fillHistograms( const LHCb::MCFTDeposit* deposit, const std::string& hitType ) const;

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
};
#endif // MCFTDEPOSITMONITOR_H
