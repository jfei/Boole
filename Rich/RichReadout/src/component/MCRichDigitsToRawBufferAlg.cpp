/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <memory>

// base class
#include "RichKernel/RichAlgBase.h"

// Event Model
#include "Event/MCRichDigit.h"
#include "Event/RawEvent.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// RICH DAQ
#include "RichFutureDAQ/EncodeTel40Data.h"
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"
#include "RichFutureDAQ/RichTel40CableMapping.h"

// RICH Utils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichSmartIDSorter.h"

namespace Rich::MC::Digi {

  // Import DAQ types
  using namespace Rich::Future::DAQ;

  //===============================================================================
  /** @class MCRichDigitsToRawBufferAlg MCRichDigitsToRawBufferAlg.h
   *
   *  Algorithm to fill the Raw buffer with RICH information from MCRichDigits.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-11-06
   */
  //===============================================================================

  class MCRichDigitsToRawBufferAlg final : public Rich::AlgBase {

  public:
    /// Standard constructor
    using Rich::AlgBase::AlgBase;

    /// Initialise
    StatusCode initialize() override final {
      // base initialise
      auto sc = Rich::AlgBase::initialize();
      if ( !sc ) return sc;

      // Force enable debug messages.
      // sc = setProperty( "OutputLevel", MSG::DEBUG );

      if ( Rich::DAQ::MaPMT1 == m_version ) {
        // Note, eventually should handle this via a derived Condition derivation
        // using the functional framework, but defer doing that as currently Boole requires
        // Raw Event object to be modified, so that would need to be changed first.

        // Load DeRichSystem
        const auto richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
        // mapping helpers
        m_pdmdbMaps = std::make_unique<PDMDBEncodeMapping>( *richSys );
        m_tel40Maps = std::make_unique<Tel40CableMapping>( *richSys );
        if ( !m_pdmdbMaps->isInitialised() ) { return Error( "Failed to initialise PDMDBEncodeMapping" ); }
        if ( !m_tel40Maps->isInitialised() ) { return Error( "Failed to initialise Tel40CableMapping" ); }

      } else if ( Rich::DAQ::StreamSmartIDs == m_version ) {
        // nothing to do ....
      } else {
        return Error( "Unknown data format version " + std::to_string( m_version.value() ) );
      }

      return sc;
    }

    /// Execution
    StatusCode execute() override final {

      // Retrieve MCRichDigits
      const auto digits = get<LHCb::MCRichDigits>( m_digitsLoc );

      // Retrieve raw Event
      auto rawEv = get<LHCb::RawEvent>( m_rawLoc );

      // Extract the Smart IDs to fill
      LHCb::RichSmartID::Vector ids;
      ids.reserve( digits->size() );
      for ( const auto dig : *digits ) { ids.emplace_back( dig->key() ); }
      // sort
      SmartIDSorter::sortByRegion( ids );

      // Format to use
      if ( Rich::DAQ::MaPMT1 == m_version ) {

        // Primary format

        // Data structure to form Tel40 banks
        EncodeTel40 encTel40( *m_tel40Maps, this );

        // loop over IDs
        for ( const auto id : ids ) {

          // get the anode data for this hit
          const auto& anodeData = m_pdmdbMaps->anodeData( id );
          // Now get the tel40 link data
          const auto& tel40Data = m_tel40Maps->tel40Data( id, anodeData.pdmdb, anodeData.frame );
          // print
          //_ri_verbo << id << endmsg;
          //_ri_verbo << " -> " << anodeData << " " << tel40Data << endmsg;

          // collect the active frame bits per Tel40 data link
          encTel40.add( tel40Data.sourceID, tel40Data.connector, anodeData.bit );
        }

        // finally loop over the collected data and form the raw banks
        encTel40.fill( *rawEv, m_version.value() );

      } else if ( Rich::DAQ::StreamSmartIDs == m_version ) {

        // Simple format that just pipes RichSmartIDs directly to a bank.
        // Does not correspond to any real data format but useful for development

        // Make a new data bank object for each Rich
        DetectorArray<PanelArray<std::vector<std::uint32_t>>> dataBanks;
        // reseve full size for each RICH panel, as best guess worst case scenario
        for ( const auto rich : Rich::detectors() ) {
          for ( const auto side : Rich::sides() ) { dataBanks[rich][side].reserve( ids.size() ); }
        }

        // Fill smartIDs direct from digits
        for ( const auto id : ids ) {
          // save in bank for correct RICH
          dataBanks[id.rich()][id.panel()].emplace_back( id );
          _ri_verbo << "Filled " << id << endmsg;
        }

        // save banks
        int iBank{0};
        for ( const auto rich : Rich::detectors() ) {
          for ( const auto side : Rich::sides() ) {
            rawEv->addBank( iBank++, LHCb::RawBank::Rich, m_version.value(), std::move( dataBanks[rich][side] ) );
          }
        }
      }

      return StatusCode::SUCCESS;
    }

  private:
    // properties

    /// Location of input MCRichDigits in TES
    Gaudi::Property<std::string> m_digitsLoc{this, "MCRichDigitsLocation", LHCb::MCRichDigitLocation::Default};

    /// Raw Event Location
    Gaudi::Property<std::string> m_rawLoc{this, "RawEventLocation", LHCb::RawEventLocation::Default};

    /// Data Format version
    Gaudi::Property<int> m_version{this, "DataVersion", Rich::DAQ::StreamSmartIDs};

  private:
    // data

    /// PDMDB Mapping data
    std::unique_ptr<PDMDBEncodeMapping> m_pdmdbMaps;

    /// Tel40 cable mapping
    std::unique_ptr<Tel40CableMapping> m_tel40Maps;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( MCRichDigitsToRawBufferAlg )

} // namespace Rich::MC::Digi
