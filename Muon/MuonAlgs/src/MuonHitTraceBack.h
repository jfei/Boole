/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCHit.h"
#include "Event/MCMuonHitHistory.h"
#include "GaudiKernel/SmartRef.h"
#include "MuonHitOrigin.h"

class MuonHitTraceBack final {
public:
  MuonHitTraceBack& setMCHit( SmartRef<LHCb::MCHit> value ) {
    m_MCMuonHit = value;
    return *this;
  }
  [[nodiscard]] const SmartRef<LHCb::MCHit>& getMCHit() const { return m_MCMuonHit; }

  MuonHitTraceBack& setHitArrivalTime( double value ) {
    m_hitArrivalTime = value;
    return *this;
  }
  [[nodiscard]] double hitArrivalTime() const { return m_hitArrivalTime; }

  [[nodiscard]] const LHCb::MCMuonHitHistory& getMCMuonHistory() const { return m_history; }
  [[nodiscard]] LHCb::MCMuonHitHistory&       getMCMuonHistory() { return m_history; }

  [[nodiscard]] MuonHitOrigin& getMCMuonHitOrigin() { return m_origin; }

  MuonHitTraceBack& setCordinate( const std::array<float, 4>& value ) {
    m_cordinate = value;
    return *this;
  }
  [[nodiscard]] const std::array<float, 4>& getCordinate() const { return m_cordinate; }

private:
  SmartRef<LHCb::MCHit>  m_MCMuonHit;
  double                 m_hitArrivalTime{0.};
  LHCb::MCMuonHitHistory m_history;
  MuonHitOrigin          m_origin{};
  std::array<float, 4>   m_cordinate{0.f, 0.f, 0.f, 0.f};
};
