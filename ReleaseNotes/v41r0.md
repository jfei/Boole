2020-02-28 Boole v41r0
===

This version uses
Lbcom [v31r0](../../Lbcom/-/tags/v31r0),
LHCb [v51r0](../../LHCb/-/tags/v51r0),
Gaudi [v33r0](../../../gaudi/Gaudi/-/tags/v33r0) and
LCG [96b](http://lcginfo.cern.ch/release/96b/) with ROOT 6.18.04.

This version is released on `master` branch.
Built relative to Boole [v40r6](../-/tags/v40r6), with the following changes:

### New features ~"new feature"

- ~RICH ~Conditions | Add possibility to use PMT channel-level properties from SIMCOND, !259 (@bmalecki)
- ~"MC checking" | Adapt BuildMCTrackInfo to Run 3, !244 (@cattanem)
- Add subdetector tests, !237 (@cattanem)


### Fixes ~"bug fix" ~workaround

- ~Decoding | Update references to follow !252, !258 (@cattanem)
- Use UT versions of ST code for UT, !252 (@graven)


### Enhancements ~enhancement

- ~RICH ~PID | Update references to new style for all tests, and follow changes due !247, !257 (@cattanem)
- ~RICH ~PID | Use more realistic pixel gain (based on tested MaPMTs)., !247 (@bmalecki)
- Upstream project highlights :star:
  - ~Build | Minimize new refs produced by validateWithRef, LHCb!2260 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Cleanup UT configuration, !246 (@cattanem)
- ~Muon ~"MC checking" | Modernize MuonAlgs, !254 (@graven)
- ~"MC checking" | Fix uninitialized variables warning, !235 (@graven)
- ~Conditions | Adapted to recent detdesc changes, !248 (@sponce)
- ~Build | Replace GaudiObjDesc generation with committed files, !230 (@clemenci) [LHCBPS-1845]
- Modernize UTDigiAlgorithms, !251 (@graven)
- Drop use of declareUTConfigProperty, use declareProperty instead, !250 (@graven)
- Fix range v3 deprecation warning, !249 (@cattanem)
- Cleanup sub-detector test options, !245 (@cattanem)
- Further cleanups for run 3, !243 (@cattanem)
- Make spillover test compatible with run 3, !242 (@cattanem)
- Remove STDigiAlgorithms from master, obsolete for Run 3, !241 (@cattanem)
- Remove OTSimulation, obsolete on master branch, !238 (@cattanem)
- Cleanup the tests, !236 (@cattanem)
- Fix unused variable, !234 (@cattanem)
- Move tests to upgrade, !233 (@sponce)
