/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file MCRichDigitSummaryAlg.cpp
 *
 * Implementation file for class : MCRichDigitSummaryAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2004-02-11
 */
//-----------------------------------------------------------------------------

// local
#include "MCRichDigitSummaryAlg.h"

// namespace
using namespace Rich::MC;

DECLARE_COMPONENT( MCRichDigitSummaryAlg )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCRichDigitSummaryAlg::MCRichDigitSummaryAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::AlgBase( name, pSvcLocator ) {
  // job options
  declareProperty( "StoreSpillover", m_storeSpill = false );
}
//=============================================================================

//=============================================================================
// initialisation
//=============================================================================
StatusCode MCRichDigitSummaryAlg::initialize() {
  // Initialize base class
  const StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // tool
  acquireTool( "RichMCTruthTool", m_truth, nullptr, true );

  if ( !m_storeSpill ) info() << "Will only store MCParticle references for main event" << endmsg;

  return sc;
}
//=============================================================================

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCRichDigitSummaryAlg::execute() {

  // Locate MCRichDigits
  LHCb::MCRichDigits* mcDigits = get<LHCb::MCRichDigits>( LHCb::MCRichDigitLocation::Default );

  // Make new container of MCRichDigitSummaries
  LHCb::MCRichDigitSummarys* summaries = new LHCb::MCRichDigitSummarys();
  put( summaries, LHCb::MCRichDigitSummaryLocation::Default );

  // loop over mc digits
  for ( LHCb::MCRichDigits::const_iterator iDig = mcDigits->begin(); iDig != mcDigits->end(); ++iDig ) {

    if ( !( *iDig )->hits().empty() ) {
      // loop over hits for this digit
      for ( LHCb::MCRichDigitHit::Vector::const_iterator iH = ( *iDig )->hits().begin(); iH != ( *iDig )->hits().end();
            ++iH ) {

        // if storing all associations or this is a "main" event hit, add to list
        const bool inMainEvent = ( *iH ).history().signalEvent();
        if ( m_storeSpill || inMainEvent ) {

          // Make and insert new summary object
          LHCb::MCRichDigitSummary* summary = new LHCb::MCRichDigitSummary();
          summaries->add( summary );

          // Set RichSmartID
          summary->setRichSmartID( ( *iDig )->richSmartID() );

          // Set MCParticle
          summary->setMCParticle( ( *iH ).mcRichHit()->mcParticle() );

          // Copy history from MCRichDigitHit ( contains simulation and digitisation history )
          summary->setHistory( ( *iH ).history() );
        }

      } // loop over hits for current digit

    } else if ( ( *iDig )->history().signalInducedNoise() ) { // special treatment for signal-induced noise digits ( no
                                                              // associated hits / MCParticle)

      // Make and insert new summary object
      LHCb::MCRichDigitSummary* summary = new LHCb::MCRichDigitSummary();
      summaries->add( summary );

      // Set RichSmartID
      summary->setRichSmartID( ( *iDig )->richSmartID() );

      // Copy history from MCRichDigit ( it is a digit without associated hits )
      summary->setHistory( ( *iDig )->history() );

    } else {
      Warning( "MCRichDigit has no MCRichHits associated" ).ignore();
    }

  } // end loop over digits

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Created " << summaries->size() << " MCRichDigitSummary objects at "
            << LHCb::MCRichDigitSummaryLocation::Default << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
