###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
if 'PYTHONSTARTUP' in os.environ:
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from array import array
import os, sys
if len(sys.argv) < 2:
    print "Not enough arguments. Usage: "
    print "   python -i clusterMonitor.py <root-file>"
    print
    sys.exit()
rootFile = sys.argv[1]

outputFile = "plots/ClusterMonitor.pdf"
os.system("mkdir -p plots")

if len(sys.argv) > 2:
    outputFile = "plots/ClusterMonitor-" + sys.argv[2] + ".pdf"

gROOT.ProcessLine(
    ".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C"
)
gStyle.SetPalette(1)
gStyle.SetMarkerSize(1.0)
gStyle.SetPadTopMargin(0.07)
gStyle.SetPadLeftMargin(0.20)
gStyle.SetTitleYOffset(1.4)

f = TFile(rootFile)


def histo(name):
    h = f.Get(name)
    if h.Class_Name() == "TObject":
        h = TH1D()
    return h


doubleGaus = TF1(
    "doubleGaus", "[0]/sqrt(2*pi)*((1-[2])*exp(-0.5*(x/[1])^2)/[1]" +
    "+[2]*exp(-0.5*(x/[3])^2)/[3])", -1.0, 1.0)
doubleGaus.SetParName(0, "#it{N}")
doubleGaus.SetParName(1, "#sigma_{core}")
doubleGaus.SetParName(2, "#it{f}_{2nd}")
doubleGaus.SetParName(3, "#sigma_{2nd}")
doubleGaus.SetLineColor(kRed)

# Plot the cluster occupancy
c5 = TCanvas("c5", "Cluster occupancy", 600, 800)
c5.Divide(2, 3)

hOccPseudo = histo("FTClusterMonitor/LiteClustersPerPseudoChannel")
hOccPseudoNoise = histo("FTClusterMonitor/Noise/LiteClustersPerPseudoChannel")
hOccPseudoSpill = histo(
    "FTClusterMonitor/Spillover/LiteClustersPerPseudoChannel")
hOccPseudoSpill.SetLineColor(kBlue)
hOccPseudoNoise.SetLineColor(kRed)
hOccChan = histo("FTClusterMonitor/LiteClustersPerChannel")
hOccChanNoise = histo("FTClusterMonitor/Noise/LiteClustersPerChannel")
hOccChanSpill = histo("FTClusterMonitor/Spillover/LiteClustersPerChannel")
hOccChanSpill.SetLineColor(kBlue)
hOccChanNoise.SetLineColor(kRed)

hOccSiPM = histo("FTClusterMonitor/LiteClustersInSiPM")

hClusCharge = histo("FTClusterMonitor/FullClusterCharge")
hClusChargeNoise = histo("FTClusterMonitor/Noise/FullClusterCharge")
hClusChargeSpill = histo("FTClusterMonitor/Spillover/FullClusterCharge")
hClusChargeSpill.SetLineColor(kBlue)
hClusChargeNoise.SetLineColor(kRed)

hClusSize = histo("FTClusterMonitor/FullClusterSize")
hClusSizeNoise = histo("FTClusterMonitor/Noise/FullClusterSize")
hClusSizeSpill = histo("FTClusterMonitor/Spillover/FullClusterSize")
hClusSizeSpill.SetLineColor(kBlue)
hClusSizeNoise.SetLineColor(kRed)

leg = TLegend(0.65, 0.60, 0.92, 0.92)
leg.SetMargin(.2)
leg.SetBorderSize(0)
leg.SetTextSize(.07)
leg.SetTextFont(132)
leg.SetLineStyle(0)
leg.SetFillColor(0)
leg.SetFillStyle(0)
leg3 = leg.Clone("leg3")
leg.AddEntry(hOccPseudo, "All", "L")
leg.AddEntry(hOccPseudoSpill, "Spillover", "L")
leg.AddEntry(hOccPseudoNoise, "Noise", "L")

c5.cd(1)
hOccPseudo.SetMinimum(0)
hOccPseudo.Draw("hist")
hOccPseudoSpill.Draw("hist same")
hOccPseudoNoise.Draw("hist same")
leg.Draw()

c5.cd(2)
hOccChan.SetMinimum(0)
hOccChan.Draw("hist")
hOccChanSpill.Draw("hist same")
hOccChanNoise.Draw("hist same")

c5.cd(3)
hOccSiPM.SetMinimum(0)
hOccSiPM.Draw("hist")
nSiPMs = hOccSiPM.GetEntries()
nSiPMsOverflow = hOccSiPM.Integral(12, 100)
latex = TLatex()
latex.DrawLatexNDC(
    0.35, 0.6,
    "#it{f}(clusters>10)= %4.2g%%" % (100 * nSiPMsOverflow / nSiPMs))

c5.cd(4)
hOccSiPMMod = {}
fracOverflow = []
modules = range(6)
for i in modules:
    hOccSiPMMod[i] = histo("FTClusterMonitor/LiteClustersInSiPM_Module" +
                           str(i))
    totEntriesMod = hOccSiPMMod[i].GetEntries()
    if (totEntriesMod == 0): fracOverflowMod = 0.0
    else: fracOverflowMod = hOccSiPMMod[i].Integral(12, 100) / totEntriesMod
    fracOverflow.append(100. * fracOverflowMod)
hFracOverflow = TH1D("hFracOverflow", "hFracOverflow", 100, -0.5, 5.5)
hFracOverflow.SetMaximum(1.2 * max(fracOverflow))
hFracOverflow.Draw("AXIS")
hFracOverflow.GetXaxis().SetTitle("Module number")
hFracOverflow.GetYaxis().SetTitle("Fraction of overflow SiPMs [%]")
grFracOverflow = TGraph(6, array('f', modules), array('f', fracOverflow))
grFracOverflow.Draw("PL")

c5.cd(5)
hClusChargeSignal = hClusCharge.Clone("hClusChargeSignal")
if (hClusChargeSignal.GetNbinsX() == hClusChargeNoise.GetNbinsX()):
    hClusChargeSignal.Add(hClusChargeNoise, -1)
if (hClusChargeSignal.GetNbinsX() == hClusChargeSpill.GetNbinsX()):
    hClusChargeSignal.Add(hClusChargeSpill, -1)
hClusChargeSignal.SetLineColor(kGray)
hClusCharge.Draw("hist")
hClusChargeNoise.Draw("hist same")
hClusChargeSpill.Draw("hist same")
hClusChargeSignal.Draw("hist same")
leg2 = leg.Clone("leg2")
leg2.AddEntry(hClusChargeSignal, "Signal", "L")
leg2.Draw()

c5.cd(6)
hClusSize.Draw("hist")
hClusSizeNoise.Draw("hist same")
hClusSizeSpill.Draw("hist same")

c5.Modified()
c5.Update()
c5.SaveAs(outputFile + "(", "pdf")

c1 = TCanvas("c1", "Cluster efficiencies", 800, 800)
c1.Divide(2, 3)

hMomTot = histo("FTClusterMonitor/HitEfficiency/MomentumMCHit")
hMomFound = histo("FTClusterMonitor/HitEfficiency/MomentumMCHitFound")
hEffMom = TEfficiency(hMomFound, hMomTot)

hETot = histo("FTClusterMonitor/HitEfficiency/EnergyLossMCHit")
hEFound = histo("FTClusterMonitor/HitEfficiency/EnergyLossMCHitFound")
hEffE = TEfficiency(hEFound, hETot)

hTimeTot = histo("FTClusterMonitor/HitEfficiency/TimeMCHit")
hTimeFound = histo("FTClusterMonitor/HitEfficiency/TimeMCHitFound")
hEffTime = TEfficiency(hTimeFound, hTimeTot)

hEff = histo("FTClusterMonitor/ClustersEfficiency5GeV")

c1.cd(1)
hEffMom.Draw()
c1.cd(2)
hEffE.Draw()
c1.cd(3)
hEffTime.Draw()
c1.cd(4)
hEff.Draw("hist")
latex.DrawLatexNDC(0.3, 0.7, "#it{p} > 5 GeV")
latex.DrawLatexNDC(0.3, 0.6, "#varepsilon = %4.2f %%" % hEff.GetMean())
c1.cd(5)
#hMomTot.Draw()
hTimeTot.Draw("hist")
c1.cd(6)
hETot.Draw("hist")

c1.Modified()
c1.Update()
c1.SaveAs(outputFile, "pdf")

# Plot the full cluster resolution
c3 = TCanvas("c3", "Lite cluster resolution", 800, 300)
c3.Divide(2, 1)

hLiteRes = {}
rmsLiteRes = []
hLiteResSmall = histo("FTClusterMonitor/Resolution/"+\
                      "LiteClusterResolutionSmall")
hLiteResLarge = histo("FTClusterMonitor/Resolution/"+\
                      "LiteClusterResolutionLarge")
c3.cd(1)
hLiteResSmall.SetFillStyle(1001)
hLiteResSmall.SetFillColor(kYellow + 1)
hLiteResSmall.Draw("hist")
rmsLiteRes.append(hLiteResSmall.GetRMS())
latex.SetTextSize(0.06)
latex.DrawLatexNDC(0.25, 0.8, "Lite clusters")
latex.DrawLatexNDC(0.65, 0.8, "Small size")
c3.cd(2)
hLiteResLarge.SetFillStyle(1001)
hLiteResLarge.SetFillColor(kYellow + 1)
hLiteResLarge.Draw("hist")
rmsLiteRes.append(hLiteResLarge.GetRMS())
latex.DrawLatexNDC(0.65, 0.8, "Large size")

c3.Modified()
c3.Update()
c3.SaveAs(outputFile, "pdf")

# Plot the full cluster resolution
c2 = TCanvas("c2", "Full cluster resolution", 800, 700)
c2.Divide(3, 3)

hFullRes = {}
rmsFullRes = []
resFullRes = []
x = range(1, 9)
for i in x:
    hFullRes[i] = histo("FTClusterMonitor/Resolution/"+\
                        "FullClusterResolutionSize"+str(i))
    c2.cd(i)
    hFullRes[i].Draw("hist")
    hFullRes[i].SetFillStyle(1001)
    hFullRes[i].SetFillColor(kYellow + 1)
    rmsFullRes.append(hFullRes[i].GetRMS())
    doubleGaus.SetParameters(hFullRes[i].GetEntries(), 0.08, 0.2, 0.5)
    hFullRes[i].Fit("doubleGaus", "Q", "", -1, 1)
    resFullRes.append(doubleGaus.GetParameter(1))
    latex.DrawLatexNDC(0.25, 0.8, "Full clusters")
    latex.DrawLatexNDC(0.65, 0.8, "Size=" + str(i))

c2.Modified()
c2.Update()
c2.SaveAs(outputFile, "pdf")

# Plot the cluster resolution
c4 = TCanvas("c4", "Cluster resolution", 800, 600)
c4.Divide(2, 2)

hLiteResTot = histo("FTClusterMonitor/Resolution/LiteClusterResolution")
hFullResTot = histo("FTClusterMonitor/Resolution/FullClusterResolution")

c4.cd(1)
hLiteResTot.Draw()
doubleGaus.SetParameters(hLiteResTot.GetEntries(), 0.08, 0.1, 0.5)
hLiteResTot.Fit("doubleGaus", "Q", "", -1, 1)
latex.DrawLatexNDC(
    0.65, 0.86,
    "#sigma_{core}= %4.1f #mum" % (1.e3 * doubleGaus.GetParameter(1)))
latex.DrawLatexNDC(
    0.65, 0.78,
    "#sigma_{2nd}= %3.0f #mum" % (1.e3 * doubleGaus.GetParameter(3)))
latex.DrawLatexNDC(0.65, 0.70,
                   "#it{f}_{2nd}= %4.2f" % doubleGaus.GetParameter(2))
latex.DrawLatexNDC(0.25, 0.8, "Lite clusters")

c4.cd(2)
hFullResTot.Draw()
doubleGaus.SetParameters(hFullResTot.GetEntries(), 0.08, 0.2, 0.5)
hFullResTot.Fit("doubleGaus", "Q", "", -1, 1)
latex.DrawLatexNDC(
    0.65, 0.86,
    "#sigma_{core}= %4.1f #mum" % (1.e3 * doubleGaus.GetParameter(1)))
latex.DrawLatexNDC(
    0.65, 0.78,
    "#sigma_{2nd}= %3.0f #mum" % (1.e3 * doubleGaus.GetParameter(3)))
latex.DrawLatexNDC(0.65, 0.70,
                   "#it{f}_{2nd}= %4.2f" % doubleGaus.GetParameter(2))
latex.DrawLatexNDC(0.25, 0.8, "Full clusters")

c4.cd(3)
xLite = [1, 2]
hRMSLite = TH1D("hRMSLite", "hRMSLite", 100, 0, 3)
hRMSLite.Draw("AXIS")
hRMSLite.GetXaxis().SetTitle("Lite cluster size")
hRMSLite.GetYaxis().SetTitle("RMS cluster resolution [mm]")
grRMSLiteRes = TGraph(2, array('f', xLite), array('f', rmsLiteRes))
grRMSLiteRes.Draw("PL")

c4.cd(4)
hRMSFull = TH1D("hRMSFull", "hRMSFull", 100, 0, 9)
hRMSFull.Draw("AXIS")
hRMSFull.GetXaxis().SetTitle("Full cluster size")
hRMSFull.GetYaxis().SetTitle("RMS cluster resolution [mm]")
grRMSFullRes = TGraph(8, array('f', x), array('f', rmsFullRes))
grRMSFullRes.Draw("PL")
grResFullRes = TGraph(8, array('f', x), array('f', resFullRes))
grResFullRes.SetLineColor(kRed)
grResFullRes.SetMarkerColor(kRed)
grResFullRes.Draw("PL")
leg3.AddEntry(grRMSFullRes, "RMS", "PL")
leg3.AddEntry(grResFullRes, "#sigma_{core}", "PL")
leg3.Draw()

c4.Modified()
c4.Update()
c4.SaveAs(outputFile, "pdf")

# Plot the cluster resolution
c6 = TCanvas("c6", "Cluster angle", 800, 600)
c6.Divide(3, 3)

hFullResAngle = {}
rmsFullResAngle = []
resFullResAngle = []
angle = ["0", "10", "20", "30", "40", "50", "60", "70", "80", "90"]
for i in range(len(angle) - 1):
    c6.cd(i + 1)
    hFullResAngle[i] = histo("FTClusterMonitor/Resolution/"+\
                        "FullClusterResolutionTheta"+angle[i+1])
    hFullResAngle[i].Draw("hist")
    hFullResAngle[i].SetFillStyle(1001)
    hFullResAngle[i].SetFillColor(kYellow + 1)
    rmsFullResAngle.append(hFullResAngle[i].GetRMS())
    doubleGaus.SetParameters(hFullResAngle[i].GetEntries(), 0.08, 0.2, 0.5)
    hFullResAngle[i].Fit("doubleGaus", "Q", "", -1, 1)
    resFullResAngle.append(doubleGaus.GetParameter(1))
    latex.DrawLatexNDC(0.25, 0.8, "Full clusters")
    latex.DrawLatexNDC(0.65, 0.8, angle[i] + " < #theta < " + angle[i + 1])

c6.Modified()
c6.Update()
c6.SaveAs(outputFile, "pdf")

# Plot the cluster resolution
c7 = TCanvas("c7", "Cluster angle", 800, 600)
c7.Divide(2, 2)

hMCHitAngle = histo("FTClusterMonitor/Resolution/MCHitTheta")
hClusterSizeVsAngle = histo(
    "FTClusterMonitor/Resolution/FullClusterSizeVsTheta")

c7.cd(1)
hMCHitAngle.SetMinimum(0.0)
hMCHitAngle.Draw("hist")

c7.cd(2)
hClusterSizeVsAngle.Draw()

c7.cd(3)
hRMSFullAngle = TH1D("hRMSFullAngle", "hRMSFullAngle", 100, 0, 90)
hRMSFullAngle.Draw("AXIS")
hRMSFullAngle.GetXaxis().SetTitle("Hit #theta [#circ]")
hRMSFullAngle.GetYaxis().SetTitle("Cluster resolution [mm]")
x = [5 + 10 * i for i in range(9)]
grRMSFullResAngle = TGraph(8, array('f', x), array('f', rmsFullResAngle))
grRMSFullResAngle.Draw("PL")
grResFullResAngle = TGraph(8, array('f', x), array('f', resFullResAngle))
grResFullResAngle.SetLineColor(kRed)
grResFullResAngle.SetMarkerColor(kRed)
grResFullResAngle.Draw("PL")
leg3.Draw()

c7.Modified()
c7.Update()
c7.SaveAs(outputFile + ")", "pdf")
