/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTCLUSTERTUPLE_H
#define FTCLUSTERTUPLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Linker
#include "Associators/Associators.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/FTLiteCluster.h"

// from MCEvent
#include "Event/MCHit.h"

/** @class FTClusterTuple FTClusterTuple.h
 *
 *
 *  @author Sevda Esen
 *  @date   2018-09-05
 */

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

class FTClusterTuple : public Gaudi::Functional::Consumer<void( const FTLiteClusters&, const std::array<SpillPair, 4>&,
                                                                const LHCb::LinksByKey& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  FTClusterTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const FTLiteClusters&, const std::array<SpillPair, 4>& spills,
                   const LHCb::LinksByKey& links ) const override;

  void fillTuple( const std::string& name, const LHCb::FTChannelID chanID, const float fraction, const float size,
                  const float isLarge, const float charge, const LHCb::LinksByKey& links ) const;

private:
  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
};
#endif // FTCLUSTERTUPLE_H
