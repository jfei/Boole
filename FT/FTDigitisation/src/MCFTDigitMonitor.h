/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTDIGITMONITOR_H
#define MCFTDIGITMONITOR_H 1

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

// from FTDet
#include "FTDet/DeFTDetector.h"

// from FTEvent
#include "Event/MCFTDigit.h"

/** @class MCFTDigitMonitor MCFTDigitMonitor.h
 *
 *
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */

class MCFTDigitMonitor : public Gaudi::Functional::Consumer<void( const LHCb::MCFTDigits& ),
                                                            Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  MCFTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::MCFTDigits& digits ) const override;

private:
  void fillHistograms( const LHCb::MCFTDigit* mcDigit, const std::set<const LHCb::MCHit*>& mcHits,
                       const std::string& hitType ) const;

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
};
#endif // MCFTDIGITMONITOR_H
