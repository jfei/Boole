/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class FTMCHitSpillMerger FTMCHitSpillMerger.h
 *
 *  This small algorithm puts the MCHits from all spills into an array
 *
 *  @author Jeroen van Tilburg
 *  @date   2017-05-11
 */

#ifndef FTMCHITSPILLMERGER_H
#define FTMCHITSPILLMERGER_H 1

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Event
#include "Event/MCHit.h"

typedef std::pair<double, const LHCb::MCHits*> SpillPair;

using namespace Gaudi::Functional;

class FTMCHitSpillMerger
    : public MergingTransformer<std::array<SpillPair, 4>( const vector_of_const_<LHCb::MCHits*>& )> {

public:
  /// Standard constructor
  FTMCHitSpillMerger( const std::string& name, ISvcLocator* pSvcLocator );

  std::array<SpillPair, 4> operator()( const vector_of_const_<LHCb::MCHits*>& mcHitsVector ) const override;

private:
  // Spill properties
  Gaudi::Property<std::vector<double>> m_spillTimes{
      this,
      "SpillTimes",
      {-50.0 * Gaudi::Units::ns, -25.0 * Gaudi::Units::ns, 0.0 * Gaudi::Units::ns, 25.0 * Gaudi::Units::ns},
      "Vector of spill arrival times"};
};
#endif // FTMCHITSPILLMERGER_H
