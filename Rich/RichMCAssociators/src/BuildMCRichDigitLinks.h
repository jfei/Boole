/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @file BuildMCRichDigitLinks.h
 *
 *  Header file for RICH DAQ algorithm : BuildMCRichDigitLinks
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-09
 */

#ifndef RICHDAQ_BUILDMCRICHDIGITLINKS_H
#define RICHDAQ_BUILDMCRICHDIGITLINKS_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// Event model
#include "Event/MCRichDigit.h"
#include "Event/MCTruth.h"
#include "Event/RichDigit.h"

namespace Rich {
  namespace MC {

    /** @class BuildMCRichDigitLinks BuildMCRichDigitLinks.h
     *
     *  Builds the links between MCRichDigits and RichDigits
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   2003-11-09
     */

    class BuildMCRichDigitLinks final : public Rich::AlgBase {

    public:
      /// Standard constructor
      BuildMCRichDigitLinks( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode execute() override final; ///< Algorithm execution

    private:
      /// String containing input MCRichDigits location in TDS
      std::string m_mcRichDigitsLocation;

      /// String containing input RichDigits location in TDS
      std::string m_richDigitsLocation;
    };

  } // namespace MC
} // namespace Rich

#endif // RICHDAQ_BUILDMCRICHDIGITLINKS_H
