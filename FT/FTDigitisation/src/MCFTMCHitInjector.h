/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCFTMCHITINJECTOR_H
#define MCFTMCHITINJECTOR_H 1

// Include files
/// from Gaudi
#include "GaudiAlg/Producer.h"

// from Event
#include "Event/MCHit.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// Detector
#include "FTDet/DeFTDetector.h"

using namespace Gaudi::Functional;

class MCFTMCHitInjector : public Producer<std::vector<LHCb::MCHit>()> {

public:
  MCFTMCHitInjector( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode               initialize() override;       ///< Algorithm initialization
  std::vector<LHCb::MCHit> operator()() const override; ///< Algorithm execution

private:
  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description

  // Properties of module to shoot at
  Gaudi::Property<std::vector<unsigned int>> m_targetFTStations{
      this, "TargetStations", {1u}, "Target Station IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTLayers{
      this, "TargetLayers", {1u}, "Target Layer IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTQuarters{
      this, "TargetQuarters", {1u}, "Target Quarter IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTModules{
      this, "TargetModules", {1u}, "Target Module IDs (unsigned int)"};
  Gaudi::Property<std::vector<unsigned int>> m_targetFTMats{this, "TargetMats", {1u}, "Target Mat IDs (unsigned int)"};

  std::vector<LHCb::FTChannelID> m_targetFTChannelIDs;

  Gaudi::Property<std::vector<double>> m_propMCHitXs{this, "MCHitLocalXs", {0.}, "MCHit X entries (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitYs{this, "MCHitLocalYs", {0.}, "MCHit Y entries (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitDeltaXs{this, "MCHitDeltaXs", {0.}, "MCHit DeltaX (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitDeltaYs{this, "MCHitDeltaYs", {0.}, "MCHit DeltaY (local)"};
  Gaudi::Property<std::vector<double>> m_propMCHitEnergies{this, "MCHitEnergies", {0.3}, "MCHit deposited energies"};
  Gaudi::Property<std::vector<double>> m_propMCHitMomenta{this, "MCHitMomenta", {150000.}, "MCHit momenta"};
  Gaudi::Property<std::vector<double>> m_propMCHitTimes{this, "MCHitTimes", {24.}, "MCHit times"};
};
#endif // MCFTMCHITINJECTOR_H
