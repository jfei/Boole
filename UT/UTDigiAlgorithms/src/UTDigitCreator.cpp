/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCTruth.h"
#include "Event/MCUTDigit.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "IUTCMSimTool.h"
#include "IUTPedestalSimTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/UTCommonBase.h"
#include "Kernel/UTDataFunctor.h"
#include "LHCbMath/LHCbMath.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_sf_erf.h"

using namespace LHCb;

/** @class UTDigitCreator UTDigitCreator.h UTAlgorithms/UTDigitCreator
 *
 *  Algorithm for creating UTDigits from MCUTDigits. The following processes
 *  are simulated:
 *  - Apply readout inefficiency/dead channels by masking MCUTDigits
 *  - Add gaussian noise to total charge of signal digits.
 *  - Generate flat noise over all readout sectors. The ADC value follows a \
 *    gaussian tail distribution.
 *  - Merge the signal and noise digits in one container.
 *  - Add neighbours to each digit with an ADC value of a gaussian core noise.
 *
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   05/01/2006
 */

class UTDigitCreator : public Gaudi::Functional::Transformer<
                           UTDigits( const MCUTDigits& mcDigitCont ),
                           Gaudi::Functional::Traits::BaseClass_t<UT::CommonBase<GaudiAlgorithm, IUTReadoutTool>>> {

public:
  // Constructor
  UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  UTDigits operator()( const MCUTDigits& mcDigitCont ) const override;

private:
  using digitPair = std::pair<double, LHCb::UTChannelID>;

  void   genRanNoiseStrips( std::vector<digitPair>& noiseCont ) const;
  void   createDigits( const LHCb::MCUTDigits* mcDigitCont, LHCb::UTDigits* digitCont ) const;
  void   mergeContainers( LHCb::span<const digitPair> noiseCont, LHCb::UTDigits* digitsCont ) const;
  void   addNeighbours( LHCb::UTDigits* digitsCont ) const;
  double genInverseTail() const;
  double adcValue( double value ) const;

  // smart interface to generators
  SmartIF<IRndmGen> m_uniformDist;
  SmartIF<IRndmGen> m_gaussDist;
  SmartIF<IRndmGen> m_gaussTailDist;

  Gaudi::Property<double> m_tailStart{this, "TailStart", 2.5};
  Gaudi::Property<double> m_saturation{this, "Saturation", 127.};
  Gaudi::Property<bool>   m_addPedestal{this, "addPedestal", false};
  Gaudi::Property<bool>   m_addCommonMode{this, "addCommonMode", false};
  Gaudi::Property<bool>   m_allStrips{this, "allStrips", false};
  Gaudi::Property<bool>   m_useStatusConditions{this, "useStatusConditions", true, "use dead strip info"};

  unsigned int m_numNoiseStrips{};

  ToolHandle<IUTPedestalSimTool> m_pedestalTool{this, "pedestalToolName", "UTPedestalSimTool"};
  ToolHandle<IUTCMSimTool>       m_cmTool{this, "cmToolName", "UTCMSimTool"};
};

DECLARE_COMPONENT_WITH_ID( UTDigitCreator, "UTDigitCreator" )

namespace {
  struct Less_by_Channel {
    template <typename Pair>
    bool operator()( const Pair& obj1, const Pair& obj2 ) const {
      return obj1.second < obj2.second;
    }
  };

  template <typename Iterator>
  std::pair<bool, Iterator> findDigit( const UTChannelID& aChan, Iterator first, Iterator last ) {
    first = std::find_if_not( first, last, [&aChan]( const auto& i ) { return i->channelID() < aChan; } );
    return {first != last && ( *first )->channelID() == aChan, first};
  }
} // namespace

double UTDigitCreator::adcValue( double value ) const {
  return LHCb::Math::round( std::min( value, m_saturation.value() ) );
}

UTDigitCreator::UTDigitCreator( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {"InputLocation", MCUTDigitLocation::UTDigits},
                  {"OutputLocation", UTDigitLocation::UTDigits}} {
  setForcedInit();
}

StatusCode UTDigitCreator::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  m_pedestalTool.setEnabled( m_addPedestal );

  m_cmTool.setEnabled( m_addCommonMode );

  // random numbers generators (flat, gaussian and gaussian tail)
  m_uniformDist   = randSvc()->generator( Rndm::Flat( 0., 1. ) );
  m_gaussDist     = randSvc()->generator( Rndm::Gauss( 0., 1. ) );
  m_gaussTailDist = randSvc()->generator( Rndm::GaussianTail( m_tailStart, 1. ) );

  // cache the number of noise strips
  if ( !m_allStrips ) {
    double fracOfNoiseStrips = 0.5 * gsl_sf_erfc( m_tailStart / sqrt( 2.0 ) );
    m_numNoiseStrips         = (int)( fracOfNoiseStrips * tracker()->nStrip() );
  } else {
    m_numNoiseStrips = tracker()->nStrip();
  }
  return sc;
}

UTDigits UTDigitCreator::operator()( const MCUTDigits& mcDigitCont ) const {

  // create UTDigits
  UTDigits digitsCont;
  createDigits( &mcDigitCont, &digitsCont );

  // generate random noise
  std::vector<digitPair> noiseCont;
  genRanNoiseStrips( noiseCont );

  // merge containers
  mergeContainers( noiseCont, &digitsCont );

  // resort by channel
  std::stable_sort( digitsCont.begin(), digitsCont.end(), UTDataFunctor::Less_by_Channel<const UTDigit*>() );

  // Ensure that there is neighbours for all strips
  addNeighbours( &digitsCont );

  // and finally resort
  std::stable_sort( digitsCont.begin(), digitsCont.end(), UTDataFunctor::Less_by_Channel<const UTDigit*>() );

  // Make the link between digits and mcdigits
  setMCTruth( &digitsCont, &mcDigitCont )
      .orThrow( "Failed to set MCTruth info", "UTDigitCreator" )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  // register UTDigits in the transient data store
  return digitsCont;
}

void UTDigitCreator::genRanNoiseStrips( std::vector<digitPair>& noiseCont ) const {
  // generate random noise strips
  noiseCont.reserve( m_numNoiseStrips );
  unsigned int nSector = tracker()->sectors().size();

  // create noise strips in loop
  const DeUTDetector::Sectors& tSectors = tracker()->sectors();
  if ( !m_allStrips ) {
    for ( unsigned int iNoiseStrip = 0u; iNoiseStrip < m_numNoiseStrips; ++iNoiseStrip ) {
      // generate a random readout sector
      int         iSector = (int)( m_uniformDist->shoot() * nSector );
      DeUTSector* aSector = tSectors[iSector];
      int         iStrip  = (int)( m_uniformDist->shoot() * aSector->nStrip() ) + 1;
      UTChannelID aChan   = aSector->stripToChan( iStrip );

      if ( m_useStatusConditions == false || aSector->isOKStrip( aChan ) == true ) {
        // Generate a ADC value following a gaussian tail distribution
        double ranNoise = aSector->noise( aChan ) * m_gaussTailDist->shoot();
        noiseCont.emplace_back( ranNoise, aChan );
      } // alive strip
    }   // iNoiseStrip
  } else {

    for ( auto tSector : tSectors ) {
      for ( unsigned int iStrip = 1u; iStrip <= tSector->nStrip(); ++iStrip ) {

        UTChannelID aChan    = tSector->stripToChan( iStrip );
        double      ranNoise = tSector->noise( aChan ) * m_gaussDist->shoot();
        noiseCont.emplace_back( ranNoise, aChan );
      } // iStrip
    }   // for
  }

  // Sort the noise digits by UTChannelID
  std::stable_sort( noiseCont.begin(), noiseCont.end(), Less_by_Channel() );
}

void UTDigitCreator::createDigits( const MCUTDigits* mcDigitCont, UTDigits* digitsCont ) const {
  // add gaussian noise to real hit channels + allow xxx% dead channels
  digitsCont->reserve( mcDigitCont->size() + m_numNoiseStrips );
  auto iterMC = mcDigitCont->begin();
  for ( ; iterMC != mcDigitCont->end(); ++iterMC ) {

    // charge including noise
    const DeUTSector*                 sector      = findSector( ( *iterMC )->channelID() );
    const SmartRefVector<MCUTDeposit> depositCont = ( *iterMC )->mcDeposit();
    double                            totalCharge = std::accumulate( depositCont.begin(), depositCont.end(), 0.,
                                          UTDataFunctor::Accumulate_Charge<const MCUTDeposit*>() );
    totalCharge += ( m_gaussDist->shoot() * sector->noise( ( *iterMC )->channelID() ) );

    // sim pedestal
    if ( m_addPedestal ) { totalCharge += m_pedestalTool->pedestal( ( *iterMC )->channelID() ); }

    // sim cm noise
    if ( m_addCommonMode ) { totalCharge += m_cmTool->noise( ( *iterMC )->channelID() ); }

    // make digit and add to container.
    digitsCont->insert( new UTDigit( adcValue( totalCharge ) ), ( *iterMC )->channelID() );
  } // iterDigit
}

void UTDigitCreator::mergeContainers( LHCb::span<const digitPair> noiseCont, UTDigits* digitsCont ) const {
  // merge the two containers
  UTChannelID prevChan( 0u, 0u, 0u, 0u, 0u, 0u );
  auto        lastIter   = digitsCont->end();
  auto        cachedIter = digitsCont->begin();

  for ( auto [totalCharge, id] : noiseCont ) {
    auto [found, iter] = findDigit( id, cachedIter, lastIter );
    cachedIter         = iter;
    // if strip was not hit: add noise
    if ( !found && prevChan != id ) {
      // sim pedestal
      if ( m_addPedestal ) totalCharge += m_pedestalTool->pedestal( id );
      // sim cm noise
      if ( m_addCommonMode ) totalCharge += m_cmTool->noise( id );
      digitsCont->insert( new UTDigit( adcValue( totalCharge ) ), id ); // does this invalidate cached & last???
    }                                                                   // findDigit
    prevChan = id;
  } // iterNoise
}

void UTDigitCreator::addNeighbours( UTDigits* digitsCont ) const {

  std::vector<digitPair> tmpCont;
  for ( auto curDigit = digitsCont->begin(); curDigit != digitsCont->end(); ++curDigit ) {

    // Get left neighbour
    const DeUTSector* aSector  = tracker()->findSector( ( *curDigit )->channelID() );
    UTChannelID       leftChan = aSector->nextLeft( ( *curDigit )->channelID() );

    // Don't add left neighbour if this neighbour is already hit
    UTChannelID prevDigitChan( 0u );
    if ( curDigit != digitsCont->begin() ) {
      auto prevDigit = std::prev( curDigit );
      prevDigitChan  = ( *prevDigit )->channelID();
    }
    if ( leftChan != prevDigitChan && leftChan != 0u ) { tmpCont.emplace_back( genInverseTail(), leftChan ); }

    // Get right neighbour
    UTChannelID rightChan = aSector->nextRight( ( *curDigit )->channelID() );

    // Don't add right neighbour if this neighbour is already hit
    UTChannelID nextDigitChan( 0u );
    auto        nextDigit = std::next( curDigit );
    if ( nextDigit != digitsCont->end() ) { nextDigitChan = ( *nextDigit )->channelID(); }
    if ( rightChan != nextDigitChan && rightChan != 0u ) { tmpCont.emplace_back( genInverseTail(), rightChan ); }
  } // iterDigit

  for ( auto [charge, id] : tmpCont ) {
    if ( !digitsCont->object( id ) ) {
      // do better sometimes we can make twice ie we start with 101
      DeUTSector* aSector = tracker()->findSector( id );
      if ( !m_useStatusConditions || aSector->isOKStrip( id ) ) {
        digitsCont->insert( new UTDigit( adcValue( charge ) ), id );
      } // ok strip
    }
  } // iterP
}

double UTDigitCreator::genInverseTail() const {
  double testVal;
  do { testVal = m_gaussDist->shoot(); } while ( testVal > m_tailStart );
  return testVal;
}
