###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##############################################################################
# File for running a Boole initialisation test with 2009 geometry
##############################################################################

from Boole.Configuration import *
from Configurables import LHCbApp

Boole().EvtMax = 0
Boole().DataType = "Upgrade"
Boole().Outputs = ["DIGI", "MDF"]  # Test all output types
Boole().Histograms = "NONE"

LHCbApp().CondDBtag = "upgrade/sim-20180530-vc-md100"
LHCbApp().DDDBtag = "upgrade/dddb-20170301"
