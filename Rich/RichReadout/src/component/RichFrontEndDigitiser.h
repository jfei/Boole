/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "RichTimeSample.h"

// This class could be removed ?

namespace Rich::MC::Digi {

  /// Utility class representing the frontend digitiser
  class RichFrontEndDigitiser {

  public:
    inline bool process( const RichTimeSample& sample, const double new_threshold ) const {
      for ( unsigned int bin = 0; bin < sample.size(); ++bin ) {
        if ( sample[bin] >= new_threshold ) { return true; }
      }
      return false;
    }
  };

} // namespace Rich::MC::Digi
