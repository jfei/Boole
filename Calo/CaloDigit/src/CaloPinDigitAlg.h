/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOPINDIGITALG_H
#define CALOPINDIGITALG_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRndmGenSvc.h"
/** @class CaloPinDigitAlg CaloPinDigitAlg.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-02-06
 */
class CaloPinDigitAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  CaloPinDigitAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloPinDigitAlg(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  IRndmGenSvc* rndmSvc() const { return m_rndmSvc; }

private:
  std::string         m_detectorName;
  std::vector<double> m_signal;
  std::vector<double> m_spread;
  DeCalorimeter*      m_calo;
  std::string         m_data;
  std::string         m_outputData;
  double              m_cNoise;
  double              m_iNoise;
  int                 m_saturateAdc;
  int                 m_rate;
  int                 m_count;
  bool                m_separatePinContainer;

  mutable IRndmGenSvc* m_rndmSvc; ///< random number service
};
#endif // CALOPINDIGITALG_H
