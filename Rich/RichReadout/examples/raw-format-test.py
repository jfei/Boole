###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
from Configurables import Boole, CondDB, LHCbApp, BooleInit

importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-SiG4EnergyDeposit.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

LHCbApp().Simulation = True
CondDB().Upgrade = True

Boole().DataType = "Upgrade"

Boole().InputDataType = "XSIM"
#Boole().Outputs = [ "DIGI" ]
#Boole().Outputs = ["MDF"]
Boole().Outputs = []
Boole().DigiType = "Extended"
#Boole().Histograms = "Expert"
#Boole().OutputLevel = DEBUG

Boole().DetectorDigi = ['Rich']
Boole().DetectorMoni = ['Rich']
Boole().DetectorLink = ['Rich']
Boole().DetectorInit = {"DATA": [], "MUON": []}

Boole().EvtMax = 10000
BooleInit().PrintFreq = 100

from Configurables import RichDigiSysConf

# Enable SIN
RichDigiSysConf().ReadParametersFromDB = True
RichDigiSysConf().Sin = True

# Set data format to realistic PMTs
RichDigiSysConf().RawDataFormatVersion = 10

# Enabled decoding test
RichDigiSysConf().TestRawFormatDecoding = True


# Enable debug messaging for data format encode and decode
def turnOnDebug():
    from Configurables import Rich__MC__Digi__MCRichDigitsToRawBufferAlg as Encode
    from Configurables import Rich__Future__RawBankDecoder as Decode
    Encode("RichFillRawBuffer").OutputLevel = 1
    Decode("RichDecodeTest").OutputLevel = 1


from Gaudi.Configuration import appendPostConfigAction
#appendPostConfigAction(turnOnDebug)

# Run command example
# gaudirun.py -T ~/LHCbCMake/Feature/Boole/Rich/RichReadout/examples/{raw-format-test.py,data.py} 2>&1 | tee Encode-Decode.log
