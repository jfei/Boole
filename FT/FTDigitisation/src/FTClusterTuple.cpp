/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "FTClusterTuple.h"
#include "Event/MCTrackInfo.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "MCInterfaces/IMCReconstructible.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterTuple
//
// 2020-05-05 : Sevda Esen
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterTuple )

FTClusterTuple::FTClusterTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"LiteClusterLocation", LHCb::FTLiteClusterLocation::Default},
                 KeyValue{"HitsLocation", "/Event/MC/FT/MergedHits"},
                 KeyValue{"LinkerLocation", Links::location( std::string( LHCb::FTLiteClusterLocation::Default ) +
                                                             "2MCHitsWithSpillover" )}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FTClusterTuple::initialize() {

  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );

  if ( m_deFT->version() < 61 ) return Error( "This version requires FTDet v6.1 or higher", StatusCode::FAILURE );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void FTClusterTuple::operator()( const FTLiteClusters& liteClusters, const std::array<SpillPair, 4>& spills,
                                 const LHCb::LinksByKey& links ) const {

  Tuple tuple   = nTuple( "FTLiteCluster" );
  Tuple mctuple = nTuple( "MCHits" );

  std::set<LHCb::MCHit*> efficientMCHits;

  auto nmax = 25;

  MCTrackInfo mcInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  // Loop over FTLiteCluster
  for ( const auto& cluster : liteClusters.range() ) {

    std::vector<float> dXCluster{};
    dXCluster.reserve( nmax );
    std::vector<float> pMCHit{};
    pMCHit.reserve( nmax );
    std::vector<float> ptMCHit{};
    ptMCHit.reserve( nmax );
    std::vector<float> etaMCHit{};
    etaMCHit.reserve( nmax );
    std::vector<int> hitTypes{};
    hitTypes.reserve( nmax );
    std::vector<int> idMCHit{};
    idMCHit.reserve( nmax );
    std::vector<int> motherMCHit{};
    motherMCHit.reserve( nmax );
    std::vector<float> txHit{};
    txHit.reserve( nmax );
    std::vector<float> tyHit{};
    tyHit.reserve( nmax );
    std::vector<float> xHit{};
    xHit.reserve( nmax );
    std::vector<float> yHit{};
    yHit.reserve( nmax );
    std::vector<float> phiHit{};
    phiHit.reserve( nmax );
    std::vector<float> thetaHit{};
    thetaHit.reserve( nmax );
    std::vector<float> distHit{};
    distHit.reserve( nmax );
    std::vector<bool>  hasT{};
    std::vector<bool>  hasVeloAndTTAndT{};
    std::vector<bool>  hasVeloAndT{};
    std::vector<bool>  hasTTAndT{};
    std::vector<float> origZ{};
    std::vector<int>   origType{};

    LHCb::FTChannelID chanID = cluster.channelID();

    // retrieve FTLiteClustertoMCHitLink
    auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>( links );

    // Get the correct module
    const DeFTModule* module = m_deFT->findModule( chanID );

    int pseudoChannel = -9999;
    if ( module != nullptr ) { pseudoChannel = module->pseudoChannel( chanID ); }

    tuple->column( "station", chanID.station() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "layer", chanID.layer() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "quarter", chanID.quarter() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "module", chanID.module() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "sipm", chanID.sipmInModule() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "channel", chanID.channel() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pseudoChannel", pseudoChannel ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    // Plots for cluster position resolution
    // Get the correct mat
    const DeFTMat* mat        = ( module ? module->findMat( chanID ) : nullptr );
    const auto     mcHitLinks = myClusterToHitLink.from( chanID );

    if ( mcHitLinks.empty() ) {

      hitTypes.push_back( 0 );
      idMCHit.push_back( -9999 );
      motherMCHit.push_back( -9999 );
      dXCluster.push_back( -9999 );
      pMCHit.push_back( -9999 );
      ptMCHit.push_back( -9999 );
      etaMCHit.push_back( -9999 );
      txHit.push_back( -9999 );
      tyHit.push_back( -9999 );
      xHit.push_back( -9999 );
      yHit.push_back( -9999 );
      thetaHit.push_back( -9999 );
      phiHit.push_back( -9999 );
      distHit.push_back( -9999 );
      origZ.push_back( -9999 );
      origType.push_back( -9999 );
      hasVeloAndTTAndT.push_back( false );
      hasVeloAndT.push_back( false );
      hasTTAndT.push_back( false );
      hasT.push_back( false );
    } else {
      // Loop over all links to MCHits
      for ( const auto& imcHit : mcHitLinks ) {
        const auto mcHit  = imcHit.to();
        const auto mcPart = ( mcHit->mcParticle() );

        int hitType = ( mcHit->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT ? 1 : 2 );

        efficientMCHits.insert( mcHit );

        hitTypes.push_back( hitType );

        idMCHit.push_back( mcPart->particleID().pid() );
        if ( mcPart->mother() )
          motherMCHit.push_back( mcPart->mother()->particleID().pid() );
        else
          motherMCHit.push_back( -9999 );

        origZ.push_back( mcPart->originVertex()->position().z() );
        origType.push_back( mcPart->originVertex()->type() );
        pMCHit.push_back( mcPart->p() );
        ptMCHit.push_back( mcPart->pt() );
        etaMCHit.push_back( mcPart->pseudoRapidity() );

        if ( mat ) {
          dXCluster.push_back( mat->distancePointToChannel( mcHit->midPoint(), chanID, cluster.fraction() ) );
          // Plot the angle of the cluster (dx) as function of its size
          Gaudi::XYZPoint localEntry = mat->geometry()->toLocal( mcHit->entry() );
          Gaudi::XYZPoint localExit  = mat->geometry()->toLocal( mcHit->exit() );
          float           tx         = ( localExit.x() - localEntry.x() ) / ( localExit.z() - localEntry.z() );
          float           theta      = atan( tx ) * 180 / Gaudi::Units::pi;
          float           ty         = ( localExit.y() - localEntry.y() ) / ( localExit.z() - localEntry.z() );
          float           phi        = atan( ty ) * 180.0f / Gaudi::Units::pi;
          float           dist       = ( localExit - localEntry ).R();

          txHit.push_back( tx );
          tyHit.push_back( ty );
          xHit.push_back( mcHit->entry().x() );
          yHit.push_back( mcHit->entry().y() );
          thetaHit.push_back( theta );
          phiHit.push_back( phi );
          distHit.push_back( dist );
        } else {
          txHit.push_back( -9999 );
          tyHit.push_back( -9999 );
          xHit.push_back( -9999 );
          yHit.push_back( -9999 );
          thetaHit.push_back( -9999 );
          phiHit.push_back( -9999 );
          distHit.push_back( -9999 );
        }
        hasVeloAndTTAndT.push_back( mcInfo.hasVeloAndT( mcPart ) && mcInfo.hasTT( mcPart ) );
        hasVeloAndT.push_back( mcInfo.hasVeloAndT( mcPart ) );
        hasTTAndT.push_back( ( mcInfo.hasT( mcPart ) && mcInfo.hasTT( mcPart ) ) );
        hasT.push_back( mcInfo.hasT( mcPart ) );
      }
    }

    tuple->column( "fraction", cluster.fraction() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pseudoSize", cluster.pseudoSize() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "resolution", dXCluster, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "pMCHit", pMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "ptMCHit", ptMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "etaMCHit", etaMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "idMCHit", idMCHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "motherMCHit", motherMCHit, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "typeHit", hitTypes, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "txHit", txHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "tyHit", tyHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "xHit", xHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "yHit", yHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "phiHit", phiHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "thetaHit", thetaHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "distHit", distHit, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origZ", origZ, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "origType", origType, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasVeloAndTTAndT", hasVeloAndTTAndT, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasVeloAndT", hasVeloAndT, "nMCHits", nmax )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasTTAndT", hasTTAndT, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->farray( "hasT", hasT, "nMCHits", nmax ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  // main loop over spills and MCHits
  for ( const auto& spill : spills ) {

    const LHCb::MCHits* mchits = spill.second;

    // Check if spill is missing
    if ( mchits == nullptr ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Spillover missing in the loop at " << spill.first << " ns" << endmsg;
      continue;
    }

    for ( auto mcHit : *mchits ) {

      bool isEfficient = efficientMCHits.find( mcHit ) != efficientMCHits.end();
      mctuple->column( "isEfficient", isEfficient ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const auto mcPart = ( mcHit->mcParticle() );

      int hitType = ( mcHit->parent()->registry()->identifier() == "/Event/" + LHCb::MCHitLocation::FT ? 1 : 2 );
      mctuple->column( "typeHit", hitType ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      mctuple->column( "pMCHit", mcPart->p() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "ptMCHit", mcPart->pt() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "etaMCHit", mcPart->pseudoRapidity() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "xHit", mcHit->entry().x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "yHit", mcHit->entry().y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      mctuple->column( "idMCHit", mcPart->particleID().pid() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "motherMCHit", mcPart->mother() != nullptr ? mcPart->mother()->particleID().pid() : -9999 )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      mctuple->column( "origZ", mcPart->originVertex()->position().z() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      mctuple->column( "origType", mcPart->originVertex()->type() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      mctuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }

  return;
}
