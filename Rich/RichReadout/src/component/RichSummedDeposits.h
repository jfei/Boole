/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//===============================================================================
/** @file RichSummedDeposits.h
 *
 *  Header file for RICH digitisation algorithm : Rich::MC::Digi::SummedDeposits
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @author Alex Howard   a.s.howard@ic.ac.uk
 *  @date   2003-11-06
 */
//===============================================================================

#ifndef RICHREADOUT_RICHSIGNAL_H
#define RICHREADOUT_RICHSIGNAL_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDeposit.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichSummedDeposit.h"

// Math
#include "GaudiKernel/Point3DTypes.h"

// kernel
#include "Kernel/ParticleID.h"

namespace Rich {
  namespace MC {
    namespace Digi {

      /** @class SummedDeposits RichSummedDeposits.h
       *
       *  Performs a simulation of the photon energy desposition
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @author Alex Howard   a.s.howard@ic.ac.uk
       *  @date   2003-11-06
       */

      class SummedDeposits final : public Rich::AlgBase {

      public:
        /// Constructor
        SummedDeposits( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override final;
        StatusCode execute() override final;
        StatusCode finalize() override final;

      private: // configuration
        Gaudi::Property<bool> m_simpleSpilloverForHPD{this, "SimpleSpilloverForHPD", true};

      private: // data
        std::string m_RichSummedDepositLocation;
        std::string m_RichDepositLocation;

        /// random number generator
        mutable Rndm::Numbers m_rndm;
      };

    } // namespace Digi
  }   // namespace MC
} // namespace Rich

#endif // RICHREADOUT_RICHSIGNAL_H
