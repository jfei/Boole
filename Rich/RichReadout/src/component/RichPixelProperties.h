/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>

#include "RichPixelReadout.h"

namespace Rich::MC::Digi {

  class RichPixel;

  /// General pixel properties
  class RichPixelProperties {

  public:
    RichPixelProperties() : m_readout( std::make_shared<RichPixelReadout>() ) {}

    inline decltype( auto ) Readout() const { return m_readout; }

  private:
    std::shared_ptr<RichPixelReadout> m_readout;

    friend class RichPixel;
  };

} // namespace Rich::MC::Digi
