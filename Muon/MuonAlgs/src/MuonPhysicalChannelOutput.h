/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCMuonDigitInfo.h"
#include "GaudiKernel/KeyedObject.h"
#include "MuonHitTraceBack.h"
#include "MuonPhChID.h"
#include "MuonPhysicalChannel.h"
#include "boost/container/static_vector.hpp"
#include <algorithm>
#include <utility>
#include <vector>

class DeMuonDetector;

class MuonPhysicalChannelOutput : public KeyedObject<int> {
public:
  MuonPhysicalChannelOutput() = default;
  explicit MuonPhysicalChannelOutput( const MuonPhysicalChannel& orig )
      : m_ID{orig.phChID()}, m_Hits{orig.hitsTraceBack()} {}
  // MuonPhysicalChannelOutput(const MuonPhysicalChannel orig){};
  MuonPhysicalChannelOutput( MuonPhChID origID, std::vector<MuonHitTraceBack> origHits )
      : m_ID{origID}, m_Hits{std::move( origHits )} {};

  /// Retrieve Stored GEANT hits connected to the ph. (const)
  [[nodiscard]] const std::vector<MuonHitTraceBack>& hitsTraceBack() const { return m_Hits; }

  /// Retrieve Stored GEANT hits connected to the ph (non-const)
  [[nodiscard]] std::vector<MuonHitTraceBack>& hitsTraceBack() { return m_Hits; }

  MuonPhysicalChannelOutput& setPhChID( MuonPhChID value ) {
    m_ID = value;
    return *this;
  }
  [[nodiscard]] const MuonPhChID& phChID() const { return m_ID; }
  [[nodiscard]] MuonPhChID&       phChID() { return m_ID; }

  MuonPhysicalChannelOutput& setMCMuonDigitInfo( LHCb::MCMuonDigitInfo value ) {
    m_phChInfo = value;
    return *this;
  }
  [[nodiscard]] LHCb::MCMuonDigitInfo&       phChInfo() { return m_phChInfo; }
  [[nodiscard]] const LHCb::MCMuonDigitInfo& phChInfo() const { return m_phChInfo; }

  MuonPhysicalChannelOutput& setFiringTime( double value ) {
    m_FiringTime = value;
    return *this;
  }
  [[nodiscard]] double firingTime() const { return m_FiringTime; }
  [[nodiscard]] boost::container::static_vector<LHCb::MuonTileID, 2>
                             calculateTileID( DeMuonDetector* muonDetector ) const;
  MuonPhysicalChannelOutput& fillTimeList();

private:
  MuonPhChID                    m_ID;
  std::vector<MuonHitTraceBack> m_Hits;
  LHCb::MCMuonDigitInfo         m_phChInfo;
  double                        m_FiringTime{0.};
  std::vector<float>            m_timeList;
};

using MuonPhysicalChannelOutputs = KeyedContainer<MuonPhysicalChannelOutput, Containers::HashMap>;
