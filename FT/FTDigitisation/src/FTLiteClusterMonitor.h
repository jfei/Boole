/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FTLITECLUSTERMONITOR_H
#define FTLITECLUSTERMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Linker
#include "Associators/Associators.h"

// FTDet
#include "FTDet/DeFTDetector.h"

// LHCbKernel
#include "Kernel/FTChannelID.h"

// from FTEvent
#include "Event/FTLiteCluster.h"

// from MCEvent
#include "Event/MCHit.h"

/** @class FTLiteClusterMonitor FTLiteClusterMonitor.h
 *
 *
 *  @author Eric Cogneras
 *  @date   2012-07-05
 */

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

class FTLiteClusterMonitor
    : public Gaudi::Functional::Consumer<void( const FTLiteClusters&, const LHCb::MCHits&, const LHCb::LinksByKey& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  FTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode finalize() override;
  void operator()( const FTLiteClusters&, const LHCb::MCHits& mcHits, const LHCb::LinksByKey& links ) const override;

private:
  Gaudi::Property<float> m_minPforResolution{this, "MinPforResolution", 0.0 * Gaudi::Units::GeV,
                                             "Minimum momentum for resolution plots"};
  Gaudi::Property<float> m_excludeSecondaries{this, "ExcludeSecondaries", true,
                                              "Exclude secondaries from calculation of efficiencies and resolutions"};

  DeFTDetector* m_deFT = nullptr; ///< pointer to FT detector description
};
#endif // FTCLUSTERMONITOR_H
