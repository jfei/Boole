/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local
#include "RichDetailedFrontEndResponsePMT.h"

using namespace Rich::MC::Digi;

//-----------------------------------------------------------------------------
// Implementation file for class : RichDetailedFrontEndResponsePMT
// 2015-10-08 : Marcin Kucharczyk, Mariusz Witek
// 2017-07-22: Bartosz Malecki
//-----------------------------------------------------------------------------
DECLARE_COMPONENT( DetailedFrontEndResponsePMT )

DetailedFrontEndResponsePMT::DetailedFrontEndResponsePMT( const std::string& name, ISvcLocator* pSvcLocator )
    : Rich::HistoAlgBase( name, pSvcLocator ) {
  setProperty( "HistoDir", "DIGI/PDRESPONSE" ).ignore();
}

StatusCode DetailedFrontEndResponsePMT::initialize() {
  StatusCode sc = Rich::HistoAlgBase::initialize();
  if ( sc.isFailure() ) { return sc; }
  // Create a collection of all valid readout channels (pixels)
  const Rich::ISmartIDTool* smartIDs( nullptr );
  acquireTool( "RichSmartIDTool", smartIDs, 0, true );
  m_readoutChannelsList = smartIDs->readoutChannelList();
  _ri_debug << "Retrieved " << m_readoutChannelsList.size() << " pixels in active list" << endmsg;
  releaseTool( smartIDs );
  // Load parameters from database
  m_propertyTool = tool<RichChannelPropertiesPMT>( "RichChannelPropertiesPMT", this );
  sc             = m_propertyTool->ReadChannelParameters( m_readoutChannelsList );
  if ( !sc ) return Error( "Failed to read channel parameters", sc );

  // Get DeRichSystem with global RICH info
  m_deRichSystem = getDetIfExists<DeRichSystem>( DeRichLocations::RichSystem );
  if ( !m_deRichSystem ) {
    error() << "Can't load the RichSystem detector element from location: " << DeRichLocations::RichSystem << endmsg;
    return StatusCode::FAILURE;
  }

  // load time info from DB
  sc = readTimeInfoPropertiesFromDB();
  if ( !sc ) { return Error( "Failed to read time info properties from the DB.", sc ); }

  // load single photoelectron deposit energy from DB
  sc = readPhotoelectronEnergyFromDB();
  if ( !sc ) { return Error( "Failed to read photoelectron deposit energy from the DB.", sc ); }

  // check if the channel properties can be read from the DB
  if ( m_readParametersFromDB.value() ) {

    const auto* firstDePMT = getDePmtFromSmartId( m_readoutChannelsList[0] );
    if ( !firstDePMT->hasCondition( DeRichLocations::PMTPropertiesCondName ) ) {

      // force to use default values (instead of reading them from DB) if the relevant condition in DB doesn't exist
      m_readParametersFromDB.setValue( false );
      _ri_debug << "Channel parameters not available in this DB version. Falling back to use the default values."
                << endmsg;
    }
  }

  // validate input for time gate
  if ( m_timeGate.value() ) {

    if ( !m_deRichSystem->hasCondition( DeRichLocations::ReadoutTimeInfoCondName ) ) {
      m_timeGate.setValue( false );
      _ri_debug << "Can't simulate time gate without access to the time info in the DB. Unsetting this option."
                << endmsg;
    }

    // validate input params
    if ( m_timeGateWindowBegin.value().size() != 2 ) {
      m_timeGate.setValue( false );
      _ri_debug << "Unexpected size of the vectors with time gate properties. The vectors should have 2 elements (one "
                   "param for each of R1/R2). Time gate will not be simulated."
                << endmsg;
    } else {

      // set the end of the time-gate window (based on the beginning and range)
      m_timeGateWindowEnd[0] = m_timeGateWindowBegin[0] + m_timeGateWindowRange;
      m_timeGateWindowEnd[1] = m_timeGateWindowBegin[1] + m_timeGateWindowRange;

      // parse and validate the lookup tables
      if ( !parseLookupTable( m_timeGateLookupTableR1.value(), m_lookupTableR1 ) ||
           !parseLookupTable( m_timeGateLookupTableR2.value(), m_lookupTableR2 ) ) {
        m_timeGate.setValue( false );
        _ri_debug
            << "Time-gate configuration is not right (the number of characters in each lookup table pattern should "
               "match the number of slots: "
            << m_timeGateNrOfSlots
            << "). Time gate "
               "will not be simulated."
            << endmsg;
      }
    }
  }

  if ( m_sin.value() ) {

    // don't simulate SIN if the channel properties from DB are not available
    if ( !m_readParametersFromDB.value() ) {

      m_sin.setValue( false );
      _ri_debug << "Can't simulate SIN without access to the channel properties in the DB. Unsetting this option."
                << endmsg;
    }
  }

  sc = m_gaussForGain.initialize( randSvc(), Rndm::Gauss( 0., 1. ) );
  if ( sc.isFailure() ) return Error( "Failed to initialize Rndm", sc );
  sc = m_flatForSinProb.initialize( randSvc(), Rndm::Flat( 0., 1. ) );
  if ( sc.isFailure() ) return Error( "Failed to initialize Rndm", sc );
  sc = m_flatForSinTime.initialize( randSvc(), Rndm::Flat( 0., m_sinTimeWindowRange ) );
  if ( sc.isFailure() ) return Error( "Failed to initialize Rndm", sc );
  sc =
      m_pdTransitTimeTypeR.initialize( randSvc(), Rndm::Gauss( m_pdTransitTimeMeanTypeR, m_pdTransitTimeSpreadTypeR ) );
  if ( sc.isFailure() ) return Error( "Failed to initialize Rndm", sc );
  sc =
      m_pdTransitTimeTypeH.initialize( randSvc(), Rndm::Gauss( m_pdTransitTimeMeanTypeH, m_pdTransitTimeSpreadTypeH ) );
  if ( sc.isFailure() ) return Error( "Failed to initialize Rndm", sc );

  return sc;
}

StatusCode DetailedFrontEndResponsePMT::finalize() {
  const auto sc = ( m_gaussForGain.finalize() && m_flatForSinProb.finalize() && m_flatForSinTime.finalize() &&
                    m_pdTransitTimeTypeR.finalize() && m_pdTransitTimeTypeH.finalize() );
  if ( !sc ) { Warning( "Failed to finalize random number generators" ).ignore(); }
  return Rich::HistoAlgBase::finalize();
}

StatusCode DetailedFrontEndResponsePMT::execute() {
  _ri_debug << "Execute" << endmsg;
  m_sDeposits = get<LHCb::MCRichSummedDeposits>( m_mcRichSumDepsLocation );
  _ri_debug << "Successfully located " << m_sDeposits->size() << " MCRichSummedDeposits at " << m_mcRichSumDepsLocation
            << endmsg;

  // container for processing channels with any deposits (signal, SIN, etc.)
  PMTChannelContainer channels;

  // Analog simulation
  StatusCode sc = Analog( channels );
  if ( sc.isFailure() ) { return sc; }
  // Digital simulation
  sc = Digital( channels );

  return sc;
}

StatusCode DetailedFrontEndResponsePMT::Analog( PMTChannelContainer& channels ) {
  _ri_debug << "Analog Simulation" << endmsg;

  // ANALOG RESPONSE
  for ( auto const& summedDeposit : *m_sDeposits ) {

    // for backward-compatibility with the spillover hits, don't process purely spillover summed deposits if the time
    // processing is not applied (it would add many irrelevant deposits) it correspond to the early spillover studies,
    // which assumed discarding hits from the main event if there was a hit in the same channel in the prev event (see
    // m_spillover) to be removed when the time-gate is optimised for the spillover studies with the full time procesing
    if ( !m_timeGate.value() ) {
      if ( !summedDeposit->history().signalEvent() ) continue;
    }

    // prepare a PMTChannel to process all deposits in the summed deposit
    PMTChannel channel{};
    channel.setSummedDeposit( summedDeposit );

    // values common for the channel
    const auto channelSmartID   = channel.getSummedDeposit()->key().pixelID();
    const auto channelThreshold = getChannelThreshold( channelSmartID );

    // process each deposit from the summed deposit
    for ( auto const& deposit : channel.getSummedDeposit()->deposits() ) {

      // equivalence of the given deposit energy in the number of initial photoelectrons (no gain simulation in Gauss)
      const int nrOfPhotoelectrons = (int)( deposit->energy() / m_photoelectronEnergy );
      // Claro input signal (number of electrons from the PMT [Me-])
      // in principle the gain value can be different for each photoelectron (but leave this 'averaged' approach, unless
      // a more detailed description is available - those gain values will not be independent of each other)
      const float inputSignal = nrOfPhotoelectrons * getChannelGain( channelSmartID );

      // save only the deposits that will be visible at the Claro output (are above threshold)
      if ( inputSignal < channelThreshold ) continue;

      // time properties of the deposit
      const auto depositTimeOfAcquisition =
          m_timeGate.value()
              ? timeOfAcquisition( deposit->time(), inputSignal, channelThreshold, channelSmartID.isLargePMT() )
              : 0.;
      const auto depositTimeOverThreshold =
          m_timeGate.value() ? getReadoutTimeOverThreshold( inputSignal, channelThreshold ) : 0.;

      channel.addDeposit( inputSignal, depositTimeOfAcquisition, depositTimeOverThreshold );
    }

    channels.insert( std::pair<LHCb::RichSmartID, PMTChannel>( channelSmartID, channel ) );
  }

  // add SIN deposits
  if ( m_sin.value() ) {

    for ( auto const& readoutChannel : m_readoutChannelsList ) {

      // scale SIN probability in a channel accordingly to the time-sampling window range
      const float sinProbabilityInChannel =
          m_timeGate.value()
              ? getChannelSinProbability( readoutChannel ) * sinProbFactorForTimeSampling( m_sinTimeWindowRange )
              : getChannelSinProbability( readoutChannel );

      // add SIN with a final probability, which is a product of the measured SIN and occupancy (all values are expected
      // to be within 0-1 range)
      if ( m_flatForSinProb() < sinProbabilityInChannel * getPmtAverageOccupancy( readoutChannel ) ) {

        // get properties of the SIN deposit
        const auto channelThreshold = getChannelThreshold( readoutChannel );

        // make sure that the inputSignal will pass the threshold (assume that we add SIN with some probability, which
        // should not be reduced by the threshold)
        const auto inputSignal = getChannelGain( readoutChannel, channelThreshold );
        // if ( inputSignal < channelThreshold ) continue; // remember to apply threshold if the SIN inputSignal will
        // not be anymore a random number above threshold value from the definition

        // time properties of the SIN deposit
        const auto timeOfAcquisition =
            m_timeGate.value()
                ? timeOfAcquisitionForSin( timeGateParam( readoutChannel, m_sinTimeWindowBegin.value() ) )
                : 0.;
        const auto timeOverThreshold =
            m_timeGate.value() ? getReadoutTimeOverThreshold( inputSignal, channelThreshold ) : 0.;

        // add the SIN deposit to the existing channel or create a new one (and get an iterator to it)
        const auto channelIteratorAndIfIsNew = channels.emplace( readoutChannel, PMTChannel() );
        channelIteratorAndIfIsNew.first->second.addDeposit( inputSignal, timeOfAcquisition, timeOverThreshold );
        channelIteratorAndIfIsNew.first->second.setHasSin();
      }
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode DetailedFrontEndResponsePMT::Digital( PMTChannelContainer& channels ) {

  _ri_debug << "Digital simulation" << endmsg;

  auto mcRichDigits = new LHCb::MCRichDigits();
  put( mcRichDigits, m_mcRichDigitsLocation );

  // Loop over all channels
  for ( auto& curChannel : channels ) {

    // process only channels with any deposit visible at the Claro output
    if ( curChannel.second.getDeposits().empty() ) continue;

    // simple spillover implementation (do not save a digit if there is any signal in this channel from prev/next event)
    if ( !m_spillover.value().compare( "PrevNext" ) ) {
      // check if summedDeposit exists (can be not the case for purely SIN channels)
      if ( curChannel.second.getSummedDeposit() != nullptr ) {
        if ( curChannel.second.getSummedDeposit()->history().prevEvent() ) continue;
        if ( curChannel.second.getSummedDeposit()->history().nextEvent() ) continue;
      }
    } else if ( !m_spillover.value().compare( "Prev" ) ) {
      // check if summedDeposit exists (can be not the case for purely SIN channels)
      if ( curChannel.second.getSummedDeposit() != nullptr ) {
        if ( curChannel.second.getSummedDeposit()->history().prevEvent() ) continue;
      }
    }

    // time processing
    if ( m_timeGate.value() ) {

      // values common for the channel
      const auto threshold          = getChannelThreshold( curChannel.first );
      const auto curTimeWindowBegin = timeGateParam( curChannel.first, m_timeGateWindowBegin.value() );
      const auto curTimeWindowEnd   = timeGateParam( curChannel.first, m_timeGateWindowEnd );
      const auto rich               = curChannel.first.rich();

      // check to make sure for the following choices on R1 / R2 lookup table / histograms (is this necessary?)
      if ( !( rich == Rich::Rich1 || rich == Rich::Rich2 ) ) { return Error( "Unrecognized RICH detector" ); }

      const auto& curLookupTable = ( rich == Rich::Rich1 ? m_lookupTableR1 : m_lookupTableR2 );

      // check all deposits in the given channel
      for ( auto const& deposit : curChannel.second.getDeposits() ) {

        if ( UNLIKELY( produceHistos() ) ) {
          const auto richAsText = Rich::text( rich );
          plot1D( deposit.inputSignal, richAsText + " : Input signal", richAsText + " : Claro input signal [Me-]", 0.,
                  m_histInputSignalMax, m_nrOfBinsInHistogram1D );
          plot1D( deposit.timeOfAcquisition, richAsText + " : Time of acqusition",
                  richAsText + " : Deposit time of acqusition [ns]", 0., m_histTimeOfAcquisitionMax,
                  m_nrOfBinsInHistogram1D );
          plot1D( deposit.timeOverThreshold, richAsText + " : Time over threshold",
                  richAsText + " : Deposit time over threshold [ns]", 0., m_histTimeOverThresholdMax,
                  m_nrOfBinsInHistogram1D );
          plot2D( deposit.inputSignal, getReadoutDelay( deposit.inputSignal, threshold ),
                  richAsText + " : Delay vs Input signal", richAsText + " : Readout delay [ns] vs Input signal [Me-]",
                  0., m_histInputSignalMax, 0., m_histReadoutDelayMax, m_nrOfBinsInHistogram2D,
                  m_nrOfBinsInHistogram2D );
          plot2D( deposit.inputSignal, deposit.timeOverThreshold, richAsText + " : TOT vs Input signal",
                  richAsText + " : TOT [ns] vs Input signal [Me-]", 0., m_histInputSignalMax, 0.,
                  m_histTimeOverThresholdMax, m_nrOfBinsInHistogram2D, m_nrOfBinsInHistogram2D );
        }

        // add the contribution from the given deposit to the Claro output pattern
        curChannel.second.processDeposit( deposit.timeOfAcquisition, deposit.timeOverThreshold, curTimeWindowBegin,
                                          curTimeWindowEnd, m_timeGateSlotWidth );
        _ri_debug << "Readout output pattern: " << curChannel.second.getOutputTimeSampling() << endmsg;
      }

      // apply a time gate on the output time-sampled signal
      const auto passesTimeGate = curChannel.second.applyTimeGate( curLookupTable );

      // print the time gate matching patterns for debugging
      if ( msgLevel( MSG::DEBUG ) ) {
        const auto timeGatePatternMatches = curChannel.second.getTimeGatePatternMatches();
        if ( timeGatePatternMatches.size() != curLookupTable.size() ) {
          debug() << "Lookup table size and number of results for compared patterns does not match. "
                  << "It will not be printed." << endmsg;
        } else {
          debug() << "Matched lookup table patterns:" << endmsg;
          for ( unsigned int i = 0; i < curLookupTable.size(); ++i ) {
            debug() << curLookupTable[i] << "\t" << timeGatePatternMatches[i] << endmsg;
          }
        }
      }

      if ( !passesTimeGate ) continue;
    }

    // create the MCRichDigit
    auto newDigit = new LHCb::MCRichDigit();
    mcRichDigits->insert( newDigit, curChannel.first );

    // check if summedDeposit exists (can be not the case for purely SIN channels)
    if ( curChannel.second.getSummedDeposit() != nullptr ) {
      const auto&                  deps = curChannel.second.getSummedDeposit()->deposits();
      LHCb::MCRichDigitHit::Vector hitVect;
      hitVect.reserve( deps.size() );
      for ( auto const& dep : deps ) { hitVect.emplace_back( *( dep->parentHit() ), dep->history() ); }
      newDigit->setHits( hitVect );
      newDigit->setHistory( curChannel.second.getSummedDeposit()->history() );
    }

    // add SIN history if necessary
    if ( curChannel.second.hasSin() ) { updateHistoryWithSin( newDigit ); }

    _ri_debug << newDigit->history() << " MCRichDigit" << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Return a valid gain for pixel
//=============================================================================
float DetailedFrontEndResponsePMT::getChannelGain( const LHCb::RichSmartID& smartID, const float& minValue ) {

  const auto channelGainMean = getChannelGainMean( smartID );
  const auto channelGainRms  = getChannelGainRms( smartID );

  // try to get valid value (maxNrTries times)
  float     tmpGain;
  const int maxNrTries = 100;
  for ( int i = 0; i < maxNrTries; ++i ) {

    tmpGain = m_gaussForGain() * channelGainRms + channelGainMean;

    if ( tmpGain > minValue )
      break;
    else {
      if ( i == 99 ) {
        tmpGain = 0.;
        warning() << "Check gain mean/sigma - no positive number in " << maxNrTries << " tries." << endmsg;
      }
    }
  }
  return tmpGain;
}

//=============================================================================
// Readout time info
//=============================================================================
StatusCode DetailedFrontEndResponsePMT::readTimeInfoPropertiesFromDB() {

  // check if the condition exists
  if ( m_deRichSystem->hasCondition( DeRichLocations::ReadoutTimeInfoCondName ) ) {

    const auto cond               = m_deRichSystem->condition( DeRichLocations::ReadoutTimeInfoCondName );
    m_pdPhotoelectronTimeOfFlight = cond->paramAsDouble( "PDPhotoelectronTimeOfFlight" );
    m_pdTransitTimeMeanTypeR      = cond->paramAsDouble( "PDTransitTimeMeanTypeR" );
    m_pdTransitTimeSpreadTypeR    = cond->paramAsDouble( "PDTransitTimeSpreadTypeR" );
    m_pdTransitTimeMeanTypeH      = cond->paramAsDouble( "PDTransitTimeMeanTypeH" );
    m_pdTransitTimeSpreadTypeH    = cond->paramAsDouble( "PDTransitTimeSpreadTypeH" );

    m_timeInfoIndexIntervalInElectrons = cond->paramAsDouble( "TimeInfoIndexIntervalInElectrons" );

    m_readoutTimeInfoIndexMin = cond->paramAsInt( "ReadoutTimeInfo_IndexMin" );
    m_readoutTimeInfoIndexMax = cond->paramAsInt( "ReadoutTimeInfo_IndexMax" );

    const auto readoutDelayPropBaseName = cond->paramAsString( "ReadoutDelayCommonLoc" );
    const auto readoutToTPropBaseName   = cond->paramAsString( "ReadoutTimeOverThresholdCommonLoc" );

    for ( unsigned int i = m_readoutTimeInfoIndexMin; i < m_readoutTimeInfoIndexMax + 1; ++i ) {

      const auto iAsString = std::to_string( i );

      SmartDataPtr<TabulatedProperty> readoutDelay( detSvc(), readoutDelayPropBaseName + iAsString );
      SmartDataPtr<TabulatedProperty> readoutToT( detSvc(), readoutToTPropBaseName + iAsString );

      m_readoutDelay.push_back(
          std::make_shared<Rich::TabulatedProperty1D>( readoutDelay, false, gsl_interp_cspline ) );
      m_readoutTimeOverThreshold.push_back(
          std::make_shared<Rich::TabulatedProperty1D>( readoutToT, false, gsl_interp_cspline ) );
    }

    // check validity of the interpolators
    for ( auto const& el : m_readoutDelay ) {

      if ( !el->valid() ) {
        error() << "Incorrect time info format. Check properties with readout delay/time-over-threshold in the DB."
                << endmsg;
        return StatusCode::FAILURE;
      }
    }

    for ( auto const& el : m_readoutTimeOverThreshold ) {

      if ( !el->valid() ) {
        error() << "Incorrect time info format. Check properties with readout delay/time-over-threshold in the DB."
                << endmsg;
        return StatusCode::FAILURE;
      }
    }

    return StatusCode::SUCCESS;

  } else {

    _ri_debug
        << "Condition with time info not available in this DB version. Any time information will be initialized to "
           "default values ('0' / nullptr)."
        << endmsg;
    return StatusCode::SUCCESS;
  }
}

float DetailedFrontEndResponsePMT::getReadoutDelay( const float inputSignal, const float threshold ) const {

  if ( m_readoutDelay.empty() ) {
    return 0.;
  } else {
    const auto readoutTimeInfoIndex = validTimeInfoInputIndex( threshold );
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( !( m_readoutDelay[readoutTimeInfoIndex]->withinInputRange( inputSignal ) ) )
        debug() << "No reliable time info for this input signal. Returned value will be from an extrapolation."
                << endmsg;
    }
    return m_readoutDelay[readoutTimeInfoIndex]->value( inputSignal );
  }
}

float DetailedFrontEndResponsePMT::getReadoutTimeOverThreshold( const float inputSignal, const float threshold ) const {

  if ( m_readoutTimeOverThreshold.empty() ) {
    return 0.;
  } else {
    const auto readoutTimeInfoIndex = validTimeInfoInputIndex( threshold );
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( !( m_readoutDelay[readoutTimeInfoIndex]->withinInputRange( inputSignal ) ) )
        debug() << "No reliable time info for this input signal. Returned value will be from an extrapolation."
                << endmsg;
    }
    return m_readoutTimeOverThreshold[readoutTimeInfoIndex]->value( inputSignal );
  }
}

// apply a time-gate on the time-sampled readout output
inline bool DetailedFrontEndResponsePMT::PMTChannel::applyTimeGate( const TimeGateLookupTable& timeGateLookupTable ) {

  auto passesTimeGate = false;

  const auto lookupTableSize = timeGateLookupTable.size();

  // ensure a clear vector of match results to the lookup patterns
  if ( !m_timeGatePatternMatches.empty() ) m_timeGatePatternMatches.clear();

  // use a standard 25 ns time gate by default
  if ( 0 == lookupTableSize ) {
    // check if any bit in the output time sampling is non-zero
    if ( m_outputTimeSampling.any() ) { passesTimeGate = true; }
  } else {
    // check if the output time-sampling pattern matches any line in the lookup table
    for ( auto const& line : timeGateLookupTable ) {

      const auto passedLine = processLookupTableLine( line, m_outputTimeSampling );
      m_timeGatePatternMatches.push_back( passedLine );

      if ( !passesTimeGate && passedLine ) { passesTimeGate = true; }
    }
  }
  return passesTimeGate;
}

//=============================================================================
// Global RICH properties
//=============================================================================
StatusCode DetailedFrontEndResponsePMT::readPhotoelectronEnergyFromDB() {

  SmartDataPtr<TabulatedProperty> photoelectronEnergyTab( detSvc(), DeRichLocations::PMTHighVoltageTabPropLoc );

  if ( !photoelectronEnergyTab ) {
    return Error( "No info on single photoelectron deposit energy found in the DB in the following location: " +
                  DeRichLocations::PMTHighVoltageTabPropLoc );
  } else {
    m_photoelectronEnergy = photoelectronEnergyTab->table().begin()->second;
    if ( !( m_photoelectronEnergy > 0. ) ) {
      error() << "Invalid value of single photoelectron deposit energy ( must be a positive number ): "
              << m_photoelectronEnergy << endmsg;
      return StatusCode::FAILURE;
    } else {
      _ri_debug << "Initialized single photoelectron deposit energy: " << m_photoelectronEnergy << endmsg;
      return StatusCode::SUCCESS;
    }
  }
}
