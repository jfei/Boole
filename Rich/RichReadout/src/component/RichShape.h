/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <cmath>
#include <map>
#include <memory>

// RichUtils
#include "RichUtils/RichMap.h"

// richdet
#include "RichDet/Rich1DTabFunc.h"

namespace Rich::MC::Digi {

  /// Rich Shape Utility class
  class RichShape {

  public:
    RichShape( const double tpeak, const double alpha ) : m_alpha( alpha ), m_tpeak( tpeak ) {

      // Initialise interpolator
      Rich::Map<double, double> data;
      data[-1] = 0; // Just to extend interpolator range to cover 0 safetly.
      for ( int i = 0; i < 101; ++i ) {
        const auto time = m_minTime + ( ( m_maxTime - m_minTime ) * i ) / 100.0;
        data[time]      = responseFunction( time );
      }
      m_tabFunc.reset( new Rich::TabulatedFunction1D( data ) );
    }

    inline double getTpeak() const { return m_tpeak; }

    inline double getPower() const { return m_alpha; }

    inline double operator[]( const double time ) const {
      // return interpolator (faster than function)
      return ( time < m_minTime || time > m_maxTime ? 0 : m_tabFunc->value( time ) );
      // return ( time < m_minTime || time > m_maxTime ? 0 : responseFunction(time) );
    }

  private: // methods
    inline double responseFunction( const double time ) const {
      return ( std::pow( time / getTpeak(), getPower() ) *
               std::exp( -( time - getTpeak() ) / ( getTpeak() * getPower() ) ) );
    }

  private: // data
    // Response function parameters
    double m_alpha{0};
    double m_tpeak{0};

    /// Max time
    double m_maxTime{200};

    /// Min time
    double m_minTime{0};

    std::unique_ptr<Rich::TabulatedFunction1D> m_tabFunc;
  };

} // namespace Rich::MC::Digi
